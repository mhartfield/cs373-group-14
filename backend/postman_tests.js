pm.test("response okay", function(){
    pm.response.to.have.status(200);
});

var responses = pm.response.json();

pm.test("Therapist exists", function(){
    pm.expect(responses).to.not.be.empty;
});

pm.test("Therapists has attributes", function(){
    pm.expect(response).to.have.property("id");
    pm.expect(response).to.have.property("Insurance");
    pm.expect(response).to.have.property("City");
});

pm.test("Facility exists", function(){
    pm.expect(responses).to.not.be.empty;
});

pm.test("Facility has attributes", function(){
    pm.expect(response).to.have.property("id");
    pm.expect(response).to.have.property("Level");
    pm.expect(response).to.have.property("City");
});

pm.test("Support Group exists", function(){
    pm.expect(responses).to.not.be.empty;
});

pm.test("Support Group has attributes", function(){
    pm.expect(response).to.have.property("id");
    pm.expect(response).to.have.property("Price");
    pm.expect(response).to.have.property("City");
});
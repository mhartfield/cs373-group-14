#!/usr/bin/env python3

import unittest
from flaskr import create_app
from flask import Flask


class TestEndpoints(unittest.TestCase):
    def setUp(self):
      app = create_app(test_config={'TESTING': True})
      self.app = app.test_client()

    def test_get_all_therapists(self):
        response = self.app.get('/Therapists/1')
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data)
        self.assertEqual(len(data), 9)

    def test_get_therapist_by_id(self):
        therapist_id = '06fe4c17-d8a1-4d23-b50e-5666ec81b5dc'
        response = self.app.get(f'/Therapists/id/{therapist_id}')
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data)

    def test_get_all_support_groups(self):
        response = self.app.get('/SupportGroups/1')
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data)
        self.assertEqual(len(data), 9)

    def test_get_support_groups_by_id(self):
        support_group_id = '4c199046-cdac-4135-b74c-f5f516280b1d'
        response = self.app.get(f'/SupportGroups/id/{support_group_id}')
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data)

    def test_get_all_facilities(self):
        response = self.app.get('/Facilities/1')
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data)
        self.assertEqual(len(data), 9)

    def test_get_facilities_by_id(self):
        facility_id = 'fc1e1a24-2e14-41e6-bbfc-c522c8d7411a'
        response = self.app.get(f'/Facilities/id/{facility_id}')
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data) 

    def test_related_therapists(self):
        location = "Austin"
        response = self.app.get(f'/RelatedTherapists/{location}')
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data) 

    def test_related_support_groups(self):
        location = "Dallas"
        response = self.app.get(f'/RelatedSupportGroups/{location}')
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data) 
    
    def test_related_facilities(self):
        location = "Houston"
        response = self.app.get(f'/RelatedFacilities/{location}')
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data) 

    def test_get_reviews_by_id(self):
        yelp_id = "baylor-scott-and-white-medical-center-temple-temple"
        response = self.app.get(f'/Reviews/{yelp_id}')
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data) 

if __name__ == "__main__":
    unittest.main()

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import inspect
from sqlalchemy.dialects.postgresql import ARRAY

db = SQLAlchemy()

class SupportGroup(db.Model):
    id = db.Column(db.Uuid, primary_key=True)
    sessionHostName = db.Column(db.String(), nullable=False)
    location = db.Column(db.String(), nullable=False)
    address = db.Column(db.String(), nullable=False)
    sessionName = db.Column(db.String(), nullable=False)
    price = db.Column(db.Integer(), nullable=False)
    traumaIssue = db.Column(ARRAY(db.String()), nullable=False)
    image = db.Column(db.String(), nullable=False)
    phone = db.Column(db.String(), nullable=False)
    sessionDesc = db.Column(db.String(), nullable=False)
    meetingTime  = db.Column(db.String(), nullable=True)
    website = db.Column(db.String(), nullable=False)

    #I tried making a base class with this method instead of redefining it for every model, but it gave me a compile error and I dont know why. SAD!
    def toDict(self):
        return { c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs }
    
class Therapist(db.Model):
    id = db.Column(db.Uuid, primary_key=True)
    name = db.Column(db.String(), nullable=False)
    insurance = db.Column(db.Boolean(), nullable=False)
    specialties = db.Column(ARRAY(db.String()), nullable=False)
    targetAgeGroup = db.Column(ARRAY(db.String()), nullable=False)
    gender = db.Column(db.String(), nullable=False)
    raceEthnicity = db.Column(db.String(), nullable=False)
    location = db.Column(db.String(), nullable=False)
    image = db.Column(db.String(), nullable=False)
    pricingService = db.Column(ARRAY(db.String()), nullable=False)
    pricingPrices = db.Column(ARRAY(db.String()), nullable=False)
    intro = db.Column(db.String(), nullable=False)
    questions = db.Column(ARRAY(db.String()), nullable=False)
    answers = db.Column(ARRAY(db.String()), nullable=False)

    def toDict(self):
        return { c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs }

class Facility(db.Model):
    id = db.Column(db.Uuid, primary_key=True)
    facilityName = db.Column(db.String(), nullable=False)
    level = db.Column(db.String(), nullable=False)
    address = db.Column(db.String(), nullable=False)
    city = db.Column(db.String(), nullable=False)
    traumaSupportArea = db.Column(db.String(), nullable=False)
    image = db.Column(db.String(), nullable=False)
    zipCode  = db.Column(db.String(), nullable=False)
    yelpID = db.Column(db.String(), nullable=False)

    def toDict(self):
        return { c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs }
    
class YelpReview(db.Model):
    id = db.Column(db.Uuid, primary_key=True)
    facility_yelpID = db.Column(db.String(), nullable=False)
    reviewText = db.Column(db.String(), nullable=False)
    user = db.Column(db.String(), nullable=False)
    stars = db.Column(db.String(), nullable=False)

    def toDict(self):
        return { c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs }

class AssociationObject(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    location = db.Column(db.String(), nullable=False)
    assoc_id = db.Column(db.Uuid, nullable=False)
    modeltype = db.Column(db.String(), nullable=False)

    def toDict(self):
        return { c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs }



    




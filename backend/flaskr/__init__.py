import os
from flask import Flask
from flask_cors import CORS
from flaskr.Models import db
import click
from dotenv import load_dotenv

load_dotenv()
username = os.getenv('PSQL_USERNAME')
password = os.getenv('PSQL_PWD')
dbname = os.getenv('PSQL_DBNAME')
URL = os.getenv('PSQL_URL')
DB_URL = f"postgresql://{username}:{password}@{URL}:5432/{dbname}"

#taken from flask tutorial website: https://flask.palletsprojects.com/en/3.0.x/tutorial/factory/
def create_app(test_config=None):
    # create and configure the app
    application = app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SQLALCHEMY_DATABASE_URI= DB_URL,
        SECRET_KEY='dev'
    )
    cors = CORS(app) # update this later

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config['TESTING'] = True
        app.config.from_mapping(test_config)
        
    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # a simple page that says hello
    db.init_app(app)
    from flaskr import SupportGroupController, TherapistController, FacilityController, YelpReviewController, AssociationObjectController
    app.register_blueprint(SupportGroupController.bp)
    app.register_blueprint(TherapistController.bp)
    app.register_blueprint(FacilityController.bp)
    app.register_blueprint(YelpReviewController.bp)
    app.register_blueprint(AssociationObjectController.bp)

    @app.route('/')
    def hello():
        return 'Testing!'
    
    @app.cli.command('createTables')
    def create_tables():
        print('Creating tables.')
        db.create_all()

    @app.cli.command('resetdb')
    def resetdb_command():
        """Destroys and creates the database + tables."""

        from sqlalchemy_utils import database_exists, create_database, drop_database
        if database_exists(DB_URL):
            print('Deleting database.')
            drop_database(DB_URL)
        if not database_exists(DB_URL):
            print('Creating database.')
            create_database(DB_URL)

        print('Creating tables.')
        db.create_all()
        print('Shiny!')

    @app.cli.command('reset-table')
    @click.option('--model', type=click.Choice(['f', 's', 't', 'y'], case_sensitive=False))
    def resetfacilitytable_command(model):

        """Destroys and creates the table specified in the --model option."""
        from flaskr import Models
        modelClass = ""
        if model == "f":
            modelClass = Models.Facility
        elif model == "s":
            modelClass = Models.SupportGroup
        elif model == "t":
            modelClass = Models.Therapist
        elif model == 'y':
            modelClass = Models.YelpReview
        else:
             raise click.BadParameter('Invalid model argument. Please use "f", "s", "y", or "t".')
        
        click.echo(f"Deleting {modelClass} table...")

        modelClass.__table__.drop(db.engine)

        db.create_all()

        click.echo(f"The {modelClass} table has been reset.")
        click.echo(f"Please update the uuid in unittests")


    return app
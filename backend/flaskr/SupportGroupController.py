from flask import Blueprint, request, jsonify
from flaskr import Models
from flaskr.Models import db
import uuid
import json
import sqlalchemy

bp = Blueprint("supportgroupcontroller", __name__)

#simple example of a get request
@bp.route('/test', methods=['GET'])
def test():
    return 'it works!'

# @bp.route('/AllSupportGroups', methods=['GET'])
# def getAllSupportGroupsTemp():
#     response = Models.SupportGroup.query.with_entities(Models.SupportGroup.id, Models.SupportGroup.location).all()
#     support_groups_data = [
#     {'id': row.id, 'location': row.location} 
#     for row in response
#     ]
#     return jsonify(support_groups_data)

#returns all support groups paginated
@bp.route('/SupportGroups/<page>',  methods=['GET'])
def getAllSupportGroups(page):
    query = sqlalchemy.select(Models.SupportGroup)
    response = db.paginate(query, page=int(page), per_page=9, error_out=False).items
    return jsonify([sg.toDict() for sg in response])

#returns one support group based on uuid
@bp.route('/SupportGroups/id/<id>', methods=['GET'])
def getSupportGroup(id):
    response = Models.SupportGroup.query.get(id).toDict()
    return jsonify(response)

# #creates a support group
# @bp.route('/SupportGroups', methods=["POST"])
# def createSupportGroup():
#     with open('scrapedData/SupportGroups.json') as json_file:
#         data = json.load(json_file)    
#     created_support_groups = []
#     for item in data:
#         id = str(uuid.uuid4())
#         new_support_group = Models.SupportGroup(
#             id = id,
#             sessionHostName = item.get("sessionHostName"),
#             location = item.get("location"),
#             address = item.get("address"),
#             sessionName = item.get("sessionName"),
#             price = item.get("price"),
#             traumaIssue = item.get("traumaIssue"),
#             image = item.get("image"),
#             phone = item.get("phone"),
#             sessionDesc = item.get("sessionDesc"),
#             meetingTime  = item.get("meetingTime"),
#             website = item.get("website"),
#         )
#         db.session.add(new_support_group)
#         created_support_groups.append(new_support_group.toDict())
#     db.session.commit()
#     return jsonify(created_support_groups)







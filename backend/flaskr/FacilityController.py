from flask import Blueprint, request, jsonify
from flaskr import Models
from flaskr.Models import db
import uuid
import json
import sqlalchemy

bp = Blueprint("facilitycontroller", __name__)

# @bp.route('/AllFacilities', methods=['GET'])
# def temp():
#     response = Models.Facility.query.with_entities(Models.Facility.id, Models.Facility.city).all()
#     support_groups_data = [
#     {'id': row.id, 'location': row.city} 
#     for row in response
#     ]
#     return jsonify(support_groups_data)

#returns all facilities paginated
@bp.route('/Facilities/<page>',  methods=['GET'])
def getAllFacilities(page):
    query = sqlalchemy.select(Models.Facility)
    response = db.paginate(query, page=int(page), per_page=9, error_out=False).items
    return jsonify([sg.toDict() for sg in response])

#returns one facility based on uuid
@bp.route('/Facilities/id/<id>', methods=['GET'])
def getFacility(id):
    response = Models.Facility.query.get(id).toDict()
    return jsonify(response)

# #creates a facility
# @bp.route('/Facilities', methods=["POST"])
# def createFacility():
#     with open('scrapedData/Facility.json') as json_file:
#         data = json.load(json_file)    
#     created_facilities = []
#     for item in data:
#         id = str(uuid.uuid4())
#         new_facility = Models.Facility(
#             id = id,
#             facilityName = item.get("facilityName"),
#             level =  item.get("level"),
#             address =  item.get("address"),
#             city = item.get("city"),
#             traumaSupportArea =  item.get("traumaSupportArea"),
#             image =  item.get("image"),
#             zipCode = str(item.get("zipCode")),
#             yelpID = item.get("yelpID"),
#             reviewUser = item.get("reviewUser"),
#             reviewRating = item.get("reviewRating"),
#             reviewText = item.get("reviewText"),
#         )
#         db.session.add(new_facility)
#         created_facilities.append(new_facility.toDict())
#     db.session.commit()
#     return jsonify(created_facilities)
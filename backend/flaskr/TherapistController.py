from flask import Blueprint, request, jsonify
from flaskr import Models
from flaskr.Models import db
import uuid
import json
import sqlalchemy

bp = Blueprint("therapistcontroller", __name__)

# @bp.route('/AllTherapists', methods=['GET'])
# def temp():
#     response = Models.Therapist.query.with_entities(Models.Therapist.id, Models.Therapist.location).all()
#     support_groups_data = [
#     {'id': row.id, 'location': row.location} 
#     for row in response
#     ]
#     return jsonify(support_groups_data)

#returns all therapists paginated
@bp.route('/Therapists/<page>',  methods=['GET'])
def getAllTherapists(page):
    query = sqlalchemy.select(Models.Therapist)
    response = db.paginate(query, page=int(page), per_page=9, error_out=False).items
    return jsonify([sg.toDict() for sg in response])

#returns one therapist based on uuid
@bp.route('/Therapists/id/<id>', methods=['GET'])
def getTherapist(id):
    response = Models.Therapist.query.get(id).toDict()
    return jsonify(response)

# #creates a therapist
# @bp.route('/Therapists', methods=["POST"])
# def createTherapist():
#     with open('scrapedData/therapists.json') as json_file:
#         data = json.load(json_file)    
#     created_therapists = []
#     for item in data:
#         id = str(uuid.uuid4())
#         new_therapist = Models.Therapist(
#             id=id,
#             name=item.get("name"),
#             insurance=True if item.get("insurance") == "Accepted" else False,
#             specialties=[item.strip() for item in item.get("specialties").split(',')],
#             targetAgeGroup=[item.strip() for item in item.get("targetAgeGroup").split(',')],
#             gender=item.get("gender"),
#             raceEthnicity=item.get("raceEthnicity"),
#             location=item.get("location",),
#             image=item.get("image"),
#             pricingService=[option["service"] for option in item.get("pricingOptions")],
#             pricingPrices=[option["price"] for option in item.get("pricingOptions")],
#             intro=item.get("intro"),
#             questions=[faq["question"] for faq in item.get("dataFaq")],
#             answers=[faq["answer"] for faq in item.get("dataFaq")]
#         )
#         db.session.add(new_therapist)
#         created_therapists.append(new_therapist.toDict())
#     db.session.commit()
#     return jsonify(created_therapists)
from flask import Blueprint, request, jsonify
from flaskr import Models
from flaskr.Models import db
import uuid
import json
import sqlalchemy

bp = Blueprint("associationobjectcontroller", __name__)

@bp.route('/RelatedTherapists/<location>', methods=['GET'])
def getRelatedT(location):
    response = Models.AssociationObject.query.filter(Models.AssociationObject.modeltype == "Therapist", Models.AssociationObject.location == location)
    data = [
    {'assoc_id': row.assoc_id} for row in response
    ]
    return jsonify(data)

@bp.route('/RelatedSupportGroups/<location>', methods=['GET'])
def getRelatedSG(location):
    response = Models.AssociationObject.query.filter(Models.AssociationObject.modeltype == "supportgroup", Models.AssociationObject.location == location)
    data = [
    {'assoc_id': row.assoc_id} for row in response
    ]
    return jsonify(data)

@bp.route('/RelatedFacilities/<location>', methods=['GET'])
def getRelatedF(location):
    response = Models.AssociationObject.query.filter(Models.AssociationObject.modeltype == "facility", Models.AssociationObject.location == location)
    data = [
    {'assoc_id': row.assoc_id} for row in response
    ]
    return jsonify(data)




#creates all ACs
# @bp.route('/AssociationObject', methods=["POST"])
# def createSupportGroup():
#     id = 0
#     created_support_groups = []
#     with open('scrapedData/currentF.json') as json_file:
#         data = json.load(json_file)    
#     for item in data:
#         id += 1
#         new = Models.AssociationObject(
#             id = id,
#             location = item.get('location').lower(),
#             assoc_id = uuid.UUID(item.get('id')),
#             modeltype = "facility"
#         )
#         db.session.add(new)
#         created_support_groups.append(new.toDict())
#     print( f"Created {len(created_support_groups)} entries.")

#     with open('scrapedData/currentSG.json') as json_file:
#         data = json.load(json_file)    
#     for item in data:
#         id += 1
#         new = Models.AssociationObject(
#             id = id,
#             location = item.get('location').lower(),
#             assoc_id = uuid.UUID(item.get('id')),
#             modeltype = "supportgroup"
#         )
#         db.session.add(new)
#         created_support_groups.append(new.toDict())
#     print( f"Created {len(created_support_groups)} entries.")

#     with open('scrapedData/currentT.json') as json_file:
#         data = json.load(json_file)    
#     for item in data:
#         id += 1
#         new = Models.AssociationObject(
#             id = id,
#             location = item.get('location').lower(),
#             assoc_id = uuid.UUID(item.get('id')),
#             modeltype = "therapist"
#         )
#         db.session.add(new)
#         created_support_groups.append(new.toDict())
#     print( f"Created {len(created_support_groups)} entries.")

#     db.session.commit()
#     return jsonify(created_support_groups)
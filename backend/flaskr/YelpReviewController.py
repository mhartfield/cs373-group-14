from flask import Blueprint, request, jsonify
from flaskr import Models
from flaskr.Models import db
import uuid
import json
import sqlalchemy

bp = Blueprint("yelpreviewcontroller", __name__)

#returns all Yelp Reviews associated with a facilityID
@bp.route('/Reviews/<id>',  methods=['GET'])
def getAllFacilities(id):
    response = Models.YelpReview.query.filter(Models.YelpReview.facility_yelpID == id)
    return jsonify([yelp.toDict() for yelp in response])

# @bp.route('/Reviews', methods=["POST"])
# def createSupportGroup():
#     with open('scrapedData/Yelp.json', errors="ignore") as json_file:
#         data = json.load(json_file)    
#     created_support_groups = []
#     for item in data:

#         yelpid = item.get("yelpID")
#         print(yelpid)
#         userList = item.get("reviewUser")
#         print(len(userList))
#         ratingList = item.get("reviewRating")
#         textList = item.get("reviewText")

#         for i in range(0, len(userList)):
#             id = str(uuid.uuid4())
#             new_yelp = Models.YelpReview(
#                 id = id,
#                 facility_yelpID = yelpid,
#                 reviewText = textList[i], 
#                 user = userList[i],
#                 stars = ratingList[i]
#             )
#             db.session.add(new_yelp)
#             created_support_groups.append(new_yelp.toDict())
#     db.session.commit()
#     return jsonify(created_support_groups)
## Canvas / Slack group number 
14

## Names of the Team Members
James You, Sierra Patton, Max Hartfield, Lucy Xu, Cooper Wilk

**Phase Leader**: James You

Responsibilites:
- Delegate tasks for each team member to focus on
- Monitor progress towards IDB #2 requirements and ensure all are met
- Help lead the back-end & API set up


|                              | Cooper | James | Lucy | Max | Sierra |
|------------------------------|--------|-------|------|-----|--------|
| Estimated Time to Completion | 15       |  15     | 15   | 15   | 17    |
| Actual Time to Completion    | 15       |  30   | 28   |  30   | 29     |

## Project Name
Texas Trauma Support

## Website
https://www.texastrauma.support

## API
**API Documentation Link:** https://documenter.getpostman.com/view/32907588/2sA2r53k1z
API Link: https://apitexastrauma.support/

## Pipeline URL
https://gitlab.com/jamesyou314/cs373-group-14/-/pipelines

## GitLab SHA
<add here>

## Phase 1:
## Project Proposal
Our platform is dedicated to aiding trauma victims in Texas by facilitating connections to vital resources and support networks. Our goal is to provide easy access to essential information and services, including counseling, support groups, and trauma facilities. By centralizing these resources, we hope to empower trauma survivors on their journey toward healing and recovery.

## Data Source URLs
- [Google Maps API (RESTful)](https://developers.google.com/maps)
- [Texas Health and Human Services](https://www.dshs.texas.gov/dshs-ems-trauma-systems/trauma-system-development/texas-trauma-facilities)
- [Psychology Today Support Groups](https://www.psychologytoday.com/us/groups/texas?category=trauma-focused)
- [MeetMonarch Trauma Therapist Directory](https://meetmonarch.com/therapists?location=Texas&specialty=Trauma)
- [Yelp API (RESTful)](https://fusion.yelp.com/)

## Model Descriptions
**Trauma Facilities**: A directory of Trauma Facilities in Texas available for trauma victims.\
**Trauma Support Groups**: A directory of Trauma Support Groups in Texas available for trauma victims.\
**Therapists**: A directory of Texas-based therapists specializing in trauma available for trauma victims.

## An estimate of the number of instances of each model:
**Trauma Facilities in Texas**: 300\
**Trauma Support Groups**: 200\
**Therapists**: 400

## Model Attributes
**Trauma Facilities**: 
- Facility Name
- Level (Comprehensive/Major/Advanced/Basic)
- Trauma Support Area
- City
- Zip code

**Trauma Support Groups**:
- Session Host Name
- Location
- Session Name
- Price
- Trauma Focus

**Therapists**:
- Name
- Insurance
- Specialties
- Target Age Group
- Gender
- Race & Ethnicity
- Location



## Model Instances
**Trauma Facilities**:
- Instances of nearby Support Groups by location
- Instances of nearby Therapists by location

**Trauma Support Groups**:
- Instances of nearby Trauma Facilities by location
- Instances of nearby Therapists by location
- Instances of Therapists with similar price range/speciality/target issue

**Therapists**:
- Instances of nearby Trauma Facilities by location
- Instances of nearby Trauma Support Groups by location
- Instances of Trauma Support Groups with similar price range


## Model Media
**Trauma Facilities**:
- Map of where facility is located
- Facility Reviews (from Yelp API)
- Photo of the facility 

**Trauma Support Groups**:
- Photo / Headshot of leader
- Map of where the group meets

**Therapists:**
- Photo / Headshot
- Graph of age groups supported
- Table of price range ```($-$$$)```

## Questions
1. Where can trauma survivors in Texas find nearby support groups tailored to their specific needs and preferences?
2. Which trauma facilities in Texas are available and equipped to provide appropriate care and services for different levels of trauma cases?
3. How can individuals locate qualified therapists in Texas who specialize in trauma therapy?

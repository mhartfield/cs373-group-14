import { useParams } from "react-router-dom";
import TraumaSupportGroupsCard from "../../components/TraumaSupportGroupsCard";
import TraumaFacilitiesCard from "../../components/TraumaFacilitiesCard";
import PricingTable from "../../components/PricingTable";
import { useEffect, useState } from "react";
import { fetchFacilitiesData, fetchSupportGroupsData, fetchTherapistData } from "../../api";

function TherapyInstance() {
  /* Gathers data on therapist from database to be later shown. */
  const { id } = useParams();
  const [therapist, setTherapist] = useState();
  const [related1, setRelated1] = useState();
  const [related2, setRelated2] = useState();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const [result, related1, related2] = await Promise.all([
          fetchTherapistData(id),
          fetchFacilitiesData(1),
          fetchSupportGroupsData(1)
        ]);

        setTherapist(result);
        setRelated1(related1)
        setRelated2(related2);
      } catch (error) {
        console.log("Error fetching API:", error);
      }
    };

    fetchData();
  }, []);

  if (!therapist) {
    return <p>Loading...</p>;
  }

  /* Returns the main formatting for instance card.
     Then scrapes pricing options from API. Finally
     links to two different instances from two different models. */
  return (
    <div className="container instance-container">
      <div className="profile-header">
        <img
          src={therapist.image}
          className="card-img-top"
          alt={`${therapist.name}'s headshot`}
        />
        <div className="profile-info">
          <h2>{therapist.name}</h2>
          <p>
            <strong>Insurance:</strong>{" "}
            {therapist.insurance ? "Accepted" : "Not Covered"}
          </p>
          <p>
            <strong>Specialties:</strong> {therapist.specialties.join(", ")}
          </p>
          <p>
            <strong>Target Age Group:</strong>{" "}
            {therapist.targetAgeGroup.join(", ")}
          </p>
          <p>
            <strong>Gender:</strong> {therapist.gender}
          </p>
          <p>
            <strong>Race & Ethnicity:</strong> {therapist.raceEthnicity}
          </p>
          <p>
            <strong>Location:</strong> {therapist.location}
          </p>
        </div>
      </div>

      <div className="summary-text">
        <h2 className="h2-section">Introduction</h2>
        <p>{therapist.intro}</p>
      </div>

      <div className="graphs">
        <h2 className="h2-section">Frequently Asked Questions</h2>
        <div>
          <div>
            {therapist.questions.map((question, index) => (
              <div key={index}>
                <h6>{question}</h6>
                <p>{therapist.answers[index]}</p>
              </div>
            ))}
          </div>
        </div>
      </div>

      <h2 className="h2-section" style={{ marginTop: "40px" }}>
        Pricing Options
      </h2>
      <PricingTable
        services={therapist.pricingService}
        prices={therapist.pricingPrices}
      />

      <div class="row graphs">
        <h2 className="h2-section">Related Support</h2>
        <div className="col">
          {related1 ? <TraumaFacilitiesCard {...related1[1]}></TraumaFacilitiesCard> : ''}
        </div>
        <div className="col">
          {related2 ?
          <TraumaSupportGroupsCard
            {...related2[1]}
          ></TraumaSupportGroupsCard> : ''}
        </div>
      </div>
    </div>
  );
}

export default TherapyInstance;

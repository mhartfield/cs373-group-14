import { useParams } from "react-router-dom";
import TherapistCard from "../../components/TherapistCard";
import TraumaFacilitiesCard from "../../components/TraumaFacilitiesCard";
import MapContainer from "../../components/MapContainer";
import { useEffect, useState } from "react";
import { fetchFacilitiesData, fetchSupportGroupData, fetchTherapistsData } from "../../api";

function SupportGroupInstance() {
  /* Gathers data on support group from database to be later shown. */
  const { id } = useParams();
  const [supportGroup, setSupportGroup] = useState();
  const [related1, setRelated1] = useState();
  const [related2, setRelated2] = useState();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const [result, related1, related2] = await Promise.all([
          fetchSupportGroupData(id),
          fetchFacilitiesData(1),
          fetchTherapistsData(1)
      ]);
        setSupportGroup(result);
        setRelated1(related1);
        setRelated2(related2);
      } catch (error) {
        console.log("Error fetching API:", error)
      }
    };
  
    fetchData();
}, []);

  if (!supportGroup) {
    return <p>Therapist not found</p>;
  }

  /* Returns the main formatting for instance card.
     Then calls Google Maps API to return map media. Finally
     links to two different instances from two different models. */
  return (
    <div className="container instance-container">
      <div className="row therapist-profile">
        <div className="row profile-header">
          <img
            src={supportGroup.image}
            className="col card-img-top"
            alt={`${supportGroup.sessionHostName}'s headshot`}
            style={{ maxHeight: "100%" }}
          />
          <div className="col profile-info">
            <h2>{supportGroup.sessionName}</h2>
            <p>
              <strong>Session Host:</strong> {supportGroup.sessionHostName}
            </p>
            <p>
              <strong>Location:</strong> {supportGroup.location}
            </p>
            <p>
              <strong>Address:</strong> {supportGroup.address}
            </p>
            <p>
              <strong>Price:</strong> ${supportGroup.price}
            </p>
            <p>
              <strong>Trauma Focus:</strong>{" "}
              {supportGroup.traumaIssue.join(", ")}
            </p>
            <p class="card-text">
              <strong>Phone Number:</strong> {supportGroup.phone}
            </p>
            <p className="card-text">
              <strong>Website:</strong>{" "}
              {supportGroup.website === "Not specified" ? (
                "N/A"
              ) : (
                <a href={supportGroup.website} target="_blank" rel="noreferrer">
                  {supportGroup.website}
                </a>
              )}
            </p>
          </div>
        </div>
        <div className="row graphs">
        <div className="col summary-text">
          <h2 className="h2-section">Session Summary</h2>
          <p>{supportGroup.sessionDesc}</p>
          <br></br>
          <p><strong>Meeting Times: </strong>{supportGroup.meetingTime}</p>
        </div>

        <div className="col summary-text">
          <h2 className="h2-section">Map Location</h2>
          <MapContainer
          location={ supportGroup.address == "Virtual" ? supportGroup.location : supportGroup.address
        }></MapContainer>
        </div>
        </div>
      </div>
          
      <div class="row graphs">
        <div></div>
        <h2 className="h2-section">Related Support</h2>
        <div className="col">
          {related1 ? <TraumaFacilitiesCard
            {...related1[1]}
          ></TraumaFacilitiesCard> : ''}
        </div>
        <div className="col">
        {related2 ?  <TherapistCard {...related2[1]}></TherapistCard> : ''}
        </div>
      </div>

    </div>
  );
}
export default SupportGroupInstance;

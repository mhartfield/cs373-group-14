import { useParams } from "react-router-dom";
import TherapistCard from "../../components/TherapistCard";
import TraumaSupportGroupsCard from "../../components/TraumaSupportGroupsCard";
import MapContainer from "../../components/MapContainer";
import YelpContainer from "../../components/YelpContainer";
import { useEffect, useState } from "react";
import { fetchFacilityData, fetchSupportGroupsData, fetchTherapistsData } from "../../api";


const FacilityInstance = () => {
  /* Gathers data on facility from database to be later shown. */
  const { id } = useParams();
  const [facility, setFacility] = useState();
  const [related1, setRelated1] = useState();
  const [related2, setRelated2] = useState();
  
  useEffect(() => {
    const fetchData = async () => {
        try {
            const [result, related1, related2] = await Promise.all([
              fetchFacilityData(id),
              fetchTherapistsData(1),
              fetchSupportGroupsData(1)
          ]);
            setFacility(result);
            setRelated1(related1);
            setRelated2(related2);
            console.log("RELATED1", related1);
            console.log("RELATED2", related2);
        }
        catch (e) {
            console.log("Error fetching API:", e)
        }
    }
    fetchData();
}, []);

  if(!facility){
    return <p>Loading...</p>
  }

  /* Returns the main formatting for instance card, along with
     Yelp Reviews from database. Then calls Google Maps API
     to return map media. Then links to two different instances 
     from two different models. */
  return (
    <div className="container instance-container">
      <div className="row therapist-profile">
        <div classname="row profile-header">
          <img
            src = {facility.image}
            className="col card-img-top"
            alt={`${facility.facilityName}'s image`}
            style={{ maxHeight: "100%" }}
          />

          <div className="col profile-info">
            <h2>{facility.facilityName}</h2>
            <p>
              <strong>Level:</strong> {facility.level}
            </p>
            <p>
              <strong>Trauma Support Area:</strong> {facility.traumaSupportArea}
            </p>
            <p>
              <strong>City:</strong> {facility.city}
            </p>
            <p>
              <strong>Address:</strong> {facility.address}
            </p>
            <p>
              <strong>Zipcode:</strong> {facility.zipCode}
            </p>
          </div>
        </div>
        <div className="row graphs">
        <div className="col summary-text">
          <h2 className="h2-section">Yelp Review</h2>
          <YelpContainer facility={facility} />
        </div>

        <div className="col summary-text">
          <h2 className="h2-section">Map Location</h2>
          <MapContainer
          location={facility.address == "Virtual" ? facility.location : facility.address
        }></MapContainer>
        </div>
        </div>

        <div class="row graphs">
          <h2 className="h2-section">Related Support</h2>
          <div className="col">
            {related1 ? <TherapistCard {...related1[1]}></TherapistCard> : "Loading..."}
          </div>
          <div className="col">
            {related2 ? <TraumaSupportGroupsCard {...related2[1]}></TraumaSupportGroupsCard> : "Loading..."}
          </div>
        </div>
      </div>
    </div>
  );
}
export default FacilityInstance;

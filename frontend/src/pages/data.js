import JhanaImg from "../images/support/Jhana.jpeg";
import LaurenImg from "../images/support/Lauren.jpeg";
import GabrielleImg from "../images/support/Gabrielle.jpeg";

/* Holds data on all therapist instances in therapist model. */
const therapistDataBlock = [
  /* Instance 1 */
  {
    name: "Sarah Osmer",
    insurance: "Accepted",
    specialties: "Trauma, Anxiety, Depression, Addiction, Adjustment Disorder",
    targetAgeGroup: "Adults",
    gender: "Female",
    raceEthnicity: "Caucasian",
    location: "Austin, TX",
    image: process.env.PUBLIC_URL + "/therapists/Osmer.jpg",
    link: "/Therapists/1",
    pricingOptions: [
      { service: "Consult Call", price: "$0" },
      { service: "Psychiatric Diagnostic Evaluation", price: "$175" },
      { service: "Psychotherapy, 60 min", price: "$150" },
      {
        service:
          "Family psychotherapy, conjoint psychotherapy with the patient present",
        price: "$175",
      },
      { service: "Group Therapy", price: "$75" },
    ],
    intro:
      "Is adulthood kicking your ass? Maybe you find that you keep repeating the same self-sabotaging behaviors and you’re just over it. Despite the fact that we’re more connected than ever before, we can be left feeling more and more isolated. You thought life was supposed to be easier than this. You’re doing the best you can, but things aren’t adding up. I believe that unconditional positive support, honesty, and humor are crucial to success, in life and therapy. Although I do currently have a short waitlist, please contact me to discuss what the length might be if you feel that we'd be a great fit!",
    dataFaq: [
      {
        question:
          "Is Sarah Osmer accepting new clients and do they offer online appointment requests?",
        answer:
          "Sarah Osmer is currently accepting new clients but has not enabled online booking.",
      },
      {
        question: "Does Sarah Osmer offer telehealth appointments?",
        answer: "Sarah Osmer does offer telehealth appointments.",
      },
      {
        question: "What areas does Sarah Osmer specialize in?",
        answer:
          "Sarah Osmer specializes in the following areas: Addiction, Adjustment Disorder, Anxiety, Attachment Issues, Bipolar Disorder, Codependency, College Mental Health, Depression, Gender Identity, Grief, LGBTQIA+, Mindfulness, Open Relationships/Polyamory, PTSD, Relationship Issues, Self Esteem, Social Anxiety, Spirituality and Religion, and Trauma.",
      },
      {
        question: "Does Sarah Osmer accept insurance, if so, what types?",
        answer:
          "Sarah Osmer does not accept insurance, however you may want to reach out to them directly to inquire about sliding scale options.",
      },
    ],
  },
  /* Instance 2 */
  {
    name: "Russ Walker",
    insurance: "Accepted",
    specialties:
      "Trauma, Anxiety, Depression, Attachment Issues, Communication Issues",
    targetAgeGroup: "Adults",
    gender: "Male",
    raceEthnicity: "Caucasian",
    location: "Austin, TX",
    image: process.env.PUBLIC_URL + "/therapists/Walker.jpg",
    link: "/Therapists/2",
    pricingOptions: [
      { service: "Consult Call", price: "$0" },
      { service: "Individual", price: "$75" },
      { service: "Couples", price: "$75" },
    ],
    intro:
      "I am a compassionate, visionary leader and expert strategist with solid financial, business, personnel development and counseling acumen based on 30+ years of experience inspiring cross-functional teams, driving organizational growth and helping individuals and couples step into the best version of themselves. I am passionate about counseling - in large part, because of how it has helped me! I approach counseling from an integrated relational framework with deep respect for both a person's strengths and passions, as well as the specific challenges a person has. I consider it a great privilege to support others in their life journey.",
    dataFaq: [
      {
        question:
          "Is Russ Walker accepting new clients and do they offer online appointment requests?",
        answer:
          "Russ Walker is currently accepting new clients and can be booked on the Monarch website. Russ Walker also offers a free consultation.",
      },
      {
        question: "Does Russ Walker offer telehealth appointments?",
        answer:
          "Russ Walker does offer telehealth appointments. You can request to book a telehealth appointment with them on their Monarch profile.",
      },
      {
        question: "What areas does Russ Walker specialize in?",
        answer:
          "Russ Walker specializes in the following areas: Anxiety, Attachment Issues, Communication Issues, Coping Skills, Depression, Divorce, Grief, Infidelity, Life Coaching, Life Transitions, Marital and Premarital, Men's Issues, Narcissistic Personality, Personality Disorder, Relationship Issues, Trauma, and Work Stress.",
      },
      {
        question: "Does Russ Walker accept insurance, if so, what types?",
        answer:
          "Russ Walker does not accept insurance, however you may want to reach out to them directly to inquire about sliding scale options.",
      },
    ],
  },
  /* Instance 3 */
  {
    name: "Esther Mutombo",
    insurance: "Accepted",
    specialties:
      "Anxiety, Depression, Trauma, Adjustment Disorder, Life Transitions",
    targetAgeGroup: "Adults",
    gender: "Female",
    raceEthnicity: "African American",
    location: "Austin, TX",
    image: process.env.PUBLIC_URL + "/therapists/Mutombo.jpg",
    link: "/Therapists/3",
    pricingOptions: [
      { service: "Consult Call", price: "$0" },
      { service: "Individual Sessions", price: "$90" },
      { service: "Couple Sessions", price: "$105" },
    ],
    intro:
      "Life's twists and turns can leave you feeling hopeless, frustrated, or lost. We all struggle with adjusting to our lives' seasons of anxiety, depression, and lingering trauma. You're not alone. Hi, I'm Esther and I will be your compassionate ally offering you the space to express and understand your experience.\n\nI offer tailored services to meet your unique and individual needs. Together, using cognitive and behavioral techniques, we will work on learning skills and strategies that will help you build resilience, cope effectively, and advance towards your brighter future.\n\nYour readiness for change is the first step, and whether its the start, middle, or end of a life chapter, we can work to help you cultivate resilience, growth, and hope. Reach out to start your journey today; I'm here to support you.",
    dataFaq: [
      {
        question:
          "Is Esther Mutombo accepting new clients and do they offer online appointment requests?",
        answer:
          "Esther Mutombo is currently accepting new clients and can be booked on the Monarch website. Esther Mutombo also offers a free consultation.",
      },
      {
        question: "Does Esther Mutombo offer telehealth appointments?",
        answer:
          "Esther Mutombo does offer telehealth appointments. You can request to book a telehealth appointment with them on their Monarch profile.",
      },
      {
        question: "What areas does Esther Mutombo specialize in?",
        answer:
          "Esther Mutombo specializes in the following areas: Adjustment Disorder, Anger Management, Anxiety, Attachment Issues, Communication Issues, Depression, Divorce, Emotional Disturbance, Life Transitions, Mood Disorders, Racial Identity, Relationship Issues, Self Esteem, Stress, Trauma, and Burnout.",
      },
      {
        question: "Does Esther Mutombo accept insurance, if so, what types?",
        answer:
          "Esther Mutombo does not accept insurance, however you may want to reach out to them directly to inquire about sliding scale options.",
      },
    ],
  },
];

const therapistData = [];
for (let i = 0; i < 200 - 1; i++) {
  therapistData.push(...therapistDataBlock);
}




















/* Holds data on all facility instances in facility model. */
const facilityData = [
  {
    /* Instance 1 */
    facilityName: "Baylor Scott & White Medical Center - Temple",
    location: "Temple, TX",
    address: "2401 S 31st St Temple, TX 76508",
    level: "Comprehensive",
    traumaSupportArea: "L",
    city: "Temple",
    zipCode: "76508",
    image: process.env.PUBLIC_URL + "/facilities/Temple.jpg",
    link: "/Facilities/1",
    stars: "1.6 out of 5 Stars",
    yelpID: "baylor-scott-and-white-medical-center-temple-temple",
  },
  /* Instance 2 */
  {
    facilityName: "Baylor University Medical Center",
    location: "Dallas, TX",
    address: "3500 Gaston Ave Dallas, TX 75246",
    level: "Comprehensive",
    traumaSupportArea: "E",
    city: "Dallas",
    zipCode: "75246",
    image: process.env.PUBLIC_URL + "/facilities/Baylor.jpg",
    link: "/Facilities/2",
    stars: "2.3 out of 5 Stars",
    yelpID: "baylor-university-medical-center-dallas-5",
  },
  /* Instance 3 */
  {
    facilityName: "Brooke Army Medical Center",
    location: "Fort Sam Houston, TX",
    address: "3551 Roger Brooke Dr, Fort Sam Houston, TX 78234",
    level: "Comprehensive",
    traumaSupportArea: "P",
    city: "Fort Sam Houston",
    zipCode: "78234",
    image: process.env.PUBLIC_URL + "/facilities/Brooke.jpg",
    link: "/Facilities/3",
    stars: "3.7 out of 5 Stars",
    yelpID: "brook-army-medical-center-fort-sam-houston",
  },
];
















/* Holds data on all support group instances in 
   support group model. */
const supportData = [
  /* Instance 1 */
  {
    sessionHostName: "Jhana Rice",
    location: "Austin, TX",
    address: "5900 Balcones Dr Suite 242 Austin, TX 78731",
    sessionName: "Adult Trauma Focused CPT Group",
    price: "$$",
    traumaIssue: ["Anxiety", "Depression"],
    image: JhanaImg,
    link: "/SupportGroups/1",
    phone: "(737) 299-8767",
    email: "hello@austinanxiety.com",
    sessionDesc:
      "This group will use Cognitive Processing Therapy (CPT) to address the impact of trauma on safety, trust, power/control, esteem, and intimacy. In 6-8 weeks, we aim to: improve understanding of PTSD, reduce distress about memories of the trauma, decrease emotional numbing (i.e., difficulty feeling feelings) and avoidance of trauma reminders, reduce feelings of being tense or “on edge”, and decrease depression, anxiety, guilt or shame to improve day-to-day living. Call 512-246-7225 or email hello@austinanxiety.com to join our next group in January 2024.",
    meetingTime: "Not displayed",
    website: "https://www.austinanxiety.com/",
  },
  /* Instance 2 */
  {
    sessionHostName: "Lauren Tepfer",
    location: "Austin, TX",
    address: "Virtual",
    sessionName: "Virtual Support Group- Interpersonal Trauma/Abuse",
    price: "$",
    traumaIssue: ["Sexual Abuse", "Trauma and PTSD"],
    image: LaurenImg,
    link: "/SupportGroups/2",
    phone: "(646) 450-3064",
    sessionDesc:
      "Interpersonal traumatic events such as sexual assault, rape, abuse from childhood, domestic or intimate partner violence, or any other experience that elicits feelings of extreme fear, shame, guilt, and/or loss of safety can cause an array of symptoms that disrupt your daily life. Often times, being a survivor of trauma can be an isolating experience, causing you to feel alone and disconnected from others who just don’t get it. This virtual support group aims to decrease feelings of isolation and provide you with a nonjudgmental and supportive space to heal in the aftermath of interpersonal trauma.",
    meetingTime: "Every Tue 6:30 P.M. - 8 P.M.",
    website: "https://www.northeastpsychological.com/",
  },
  /* Instaance 3 */
  {
    sessionHostName: "Gabrielle Coolidge",
    location: "Austin, TX",
    address: "706 West Ben White Boulevard B220 Austin, TX 78704",
    sessionName: "Phoenix Rising",
    price: "$",
    traumaIssue: ["Divorce", "Trauma and PTSD", "Women's Issues"],
    image: GabrielleImg,
    link: "/SupportGroups/3",
    phone: "(512) 955-5847",
    sessionDesc:
      "Process group for individuals who are survivors of Narcissistic Abuse. This group is for individuals who are seeking support from people who 'get it', and have a safe space to process through the trauma of being in a narcissistic relationship. This group helps its members identify the 'why' of finding themselves in such a relationship and how to protect themselves from entering another one in the future.",
    meetingTime: "Every Tue 6 P.M. - 7:30 P.M.",
    website: "N/A",
  },

  {
    sessionHostName: "Jhana Rice",
    location: "Austin, TX",
    address: "5900 Balcones Dr Suite 242 Austin, TX 78731",
    sessionName: "Adult Trauma Focused CPT Group",
    price: "$$",
    traumaIssue: ["Anxiety", "Depression"],
    image: JhanaImg,
    link: "/SupportGroups/1",
    phone: "(737) 299-8767",
    email: "hello@austinanxiety.com",
    sessionDesc:
      "This group will use Cognitive Processing Therapy (CPT) to address the impact of trauma on safety, trust, power/control, esteem, and intimacy. In 6-8 weeks, we aim to: improve understanding of PTSD, reduce distress about memories of the trauma, decrease emotional numbing (i.e., difficulty feeling feelings) and avoidance of trauma reminders, reduce feelings of being tense or “on edge”, and decrease depression, anxiety, guilt or shame to improve day-to-day living. Call 512-246-7225 or email hello@austinanxiety.com to join our next group in January 2024.",
    meetingTime: "Not displayed",
    website: "https://www.austinanxiety.com/",
  },
  /* Instance 2 */
  {
    sessionHostName: "Lauren Tepfer",
    location: "Austin, TX",
    address: "Virtual",
    sessionName: "Virtual Support Group- Interpersonal Trauma/Abuse",
    price: "$",
    traumaIssue: ["Sexual Abuse", "Trauma and PTSD"],
    image: LaurenImg,
    link: "/SupportGroups/2",
    phone: "(646) 450-3064",
    sessionDesc:
      "Interpersonal traumatic events such as sexual assault, rape, abuse from childhood, domestic or intimate partner violence, or any other experience that elicits feelings of extreme fear, shame, guilt, and/or loss of safety can cause an array of symptoms that disrupt your daily life. Often times, being a survivor of trauma can be an isolating experience, causing you to feel alone and disconnected from others who just don’t get it. This virtual support group aims to decrease feelings of isolation and provide you with a nonjudgmental and supportive space to heal in the aftermath of interpersonal trauma.",
    meetingTime: "Every Tue 6:30 P.M. - 8 P.M.",
    website: "https://www.northeastpsychological.com/",
  },
  /* Instaance 3 */
  {
    sessionHostName: "Gabrielle Coolidge",
    location: "Austin, TX",
    address: "706 West Ben White Boulevard B220 Austin, TX 78704",
    sessionName: "Phoenix Rising",
    price: "$",
    traumaIssue: ["Divorce", "Trauma and PTSD", "Women's Issues"],
    image: GabrielleImg,
    link: "/SupportGroups/3",
    phone: "(512) 955-5847",
    sessionDesc:
      "Process group for individuals who are survivors of Narcissistic Abuse. This group is for individuals who are seeking support from people who 'get it', and have a safe space to process through the trauma of being in a narcissistic relationship. This group helps its members identify the 'why' of finding themselves in such a relationship and how to protect themselves from entering another one in the future.",
    meetingTime: "Every Tue 6 P.M. - 7:30 P.M.",
    website: "N/A",
  },

  {
    sessionHostName: "Jhana Rice",
    location: "Austin, TX",
    address: "5900 Balcones Dr Suite 242 Austin, TX 78731",
    sessionName: "Adult Trauma Focused CPT Group",
    price: "$$",
    traumaIssue: ["Anxiety", "Depression"],
    image: JhanaImg,
    link: "/SupportGroups/1",
    phone: "(737) 299-8767",
    email: "hello@austinanxiety.com",
    sessionDesc:
      "This group will use Cognitive Processing Therapy (CPT) to address the impact of trauma on safety, trust, power/control, esteem, and intimacy. In 6-8 weeks, we aim to: improve understanding of PTSD, reduce distress about memories of the trauma, decrease emotional numbing (i.e., difficulty feeling feelings) and avoidance of trauma reminders, reduce feelings of being tense or “on edge”, and decrease depression, anxiety, guilt or shame to improve day-to-day living. Call 512-246-7225 or email hello@austinanxiety.com to join our next group in January 2024.",
    meetingTime: "Not displayed",
    website: "https://www.austinanxiety.com/",
  },
  /* Instance 2 */
  {
    sessionHostName: "Lauren Tepfer",
    location: "Austin, TX",
    address: "Virtual",
    sessionName: "Virtual Support Group- Interpersonal Trauma/Abuse",
    price: "$",
    traumaIssue: ["Sexual Abuse", "Trauma and PTSD"],
    image: LaurenImg,
    link: "/SupportGroups/2",
    phone: "(646) 450-3064",
    sessionDesc:
      "Interpersonal traumatic events such as sexual assault, rape, abuse from childhood, domestic or intimate partner violence, or any other experience that elicits feelings of extreme fear, shame, guilt, and/or loss of safety can cause an array of symptoms that disrupt your daily life. Often times, being a survivor of trauma can be an isolating experience, causing you to feel alone and disconnected from others who just don’t get it. This virtual support group aims to decrease feelings of isolation and provide you with a nonjudgmental and supportive space to heal in the aftermath of interpersonal trauma.",
    meetingTime: "Every Tue 6:30 P.M. - 8 P.M.",
    website: "https://www.northeastpsychological.com/",
  },
  /* Instaance 3 */
  {
    sessionHostName: "Gabrielle Coolidge",
    location: "Austin, TX",
    address: "706 West Ben White Boulevard B220 Austin, TX 78704",
    sessionName: "Phoenix Rising",
    price: "$",
    traumaIssue: ["Divorce", "Trauma and PTSD", "Women's Issues"],
    image: GabrielleImg,
    link: "/SupportGroups/3",
    phone: "(512) 955-5847",
    sessionDesc:
      "Process group for individuals who are survivors of Narcissistic Abuse. This group is for individuals who are seeking support from people who 'get it', and have a safe space to process through the trauma of being in a narcissistic relationship. This group helps its members identify the 'why' of finding themselves in such a relationship and how to protect themselves from entering another one in the future.",
    meetingTime: "Every Tue 6 P.M. - 7:30 P.M.",
    website: "N/A",
  },

  {
    sessionHostName: "Jhana Rice",
    location: "Austin, TX",
    address: "5900 Balcones Dr Suite 242 Austin, TX 78731",
    sessionName: "Adult Trauma Focused CPT Group",
    price: "$$",
    traumaIssue: ["Anxiety", "Depression"],
    image: JhanaImg,
    link: "/SupportGroups/1",
    phone: "(737) 299-8767",
    email: "hello@austinanxiety.com",
    sessionDesc:
      "This group will use Cognitive Processing Therapy (CPT) to address the impact of trauma on safety, trust, power/control, esteem, and intimacy. In 6-8 weeks, we aim to: improve understanding of PTSD, reduce distress about memories of the trauma, decrease emotional numbing (i.e., difficulty feeling feelings) and avoidance of trauma reminders, reduce feelings of being tense or “on edge”, and decrease depression, anxiety, guilt or shame to improve day-to-day living. Call 512-246-7225 or email hello@austinanxiety.com to join our next group in January 2024.",
    meetingTime: "Not displayed",
    website: "https://www.austinanxiety.com/",
  },
  /* Instance 2 */
  {
    sessionHostName: "Lauren Tepfer",
    location: "Austin, TX",
    address: "Virtual",
    sessionName: "Virtual Support Group- Interpersonal Trauma/Abuse",
    price: "$",
    traumaIssue: ["Sexual Abuse", "Trauma and PTSD"],
    image: LaurenImg,
    link: "/SupportGroups/2",
    phone: "(646) 450-3064",
    sessionDesc:
      "Interpersonal traumatic events such as sexual assault, rape, abuse from childhood, domestic or intimate partner violence, or any other experience that elicits feelings of extreme fear, shame, guilt, and/or loss of safety can cause an array of symptoms that disrupt your daily life. Often times, being a survivor of trauma can be an isolating experience, causing you to feel alone and disconnected from others who just don’t get it. This virtual support group aims to decrease feelings of isolation and provide you with a nonjudgmental and supportive space to heal in the aftermath of interpersonal trauma.",
    meetingTime: "Every Tue 6:30 P.M. - 8 P.M.",
    website: "https://www.northeastpsychological.com/",
  },
  /* Instaance 3 */
  {
    sessionHostName: "Gabrielle Coolidge",
    location: "Austin, TX",
    address: "706 West Ben White Boulevard B220 Austin, TX 78704",
    sessionName: "Phoenix Rising",
    price: "$",
    traumaIssue: ["Divorce", "Trauma and PTSD", "Women's Issues"],
    image: GabrielleImg,
    link: "/SupportGroups/3",
    phone: "(512) 955-5847",
    sessionDesc:
      "Process group for individuals who are survivors of Narcissistic Abuse. This group is for individuals who are seeking support from people who 'get it', and have a safe space to process through the trauma of being in a narcissistic relationship. This group helps its members identify the 'why' of finding themselves in such a relationship and how to protect themselves from entering another one in the future.",
    meetingTime: "Every Tue 6 P.M. - 7:30 P.M.",
    website: "N/A",
  },

  {
    sessionHostName: "Jhana Rice",
    location: "Austin, TX",
    address: "5900 Balcones Dr Suite 242 Austin, TX 78731",
    sessionName: "Adult Trauma Focused CPT Group",
    price: "$$",
    traumaIssue: ["Anxiety", "Depression"],
    image: JhanaImg,
    link: "/SupportGroups/1",
    phone: "(737) 299-8767",
    email: "hello@austinanxiety.com",
    sessionDesc:
      "This group will use Cognitive Processing Therapy (CPT) to address the impact of trauma on safety, trust, power/control, esteem, and intimacy. In 6-8 weeks, we aim to: improve understanding of PTSD, reduce distress about memories of the trauma, decrease emotional numbing (i.e., difficulty feeling feelings) and avoidance of trauma reminders, reduce feelings of being tense or “on edge”, and decrease depression, anxiety, guilt or shame to improve day-to-day living. Call 512-246-7225 or email hello@austinanxiety.com to join our next group in January 2024.",
    meetingTime: "Not displayed",
    website: "https://www.austinanxiety.com/",
  },
  /* Instance 2 */
  {
    sessionHostName: "Lauren Tepfer",
    location: "Austin, TX",
    address: "Virtual",
    sessionName: "Virtual Support Group- Interpersonal Trauma/Abuse",
    price: "$",
    traumaIssue: ["Sexual Abuse", "Trauma and PTSD"],
    image: LaurenImg,
    link: "/SupportGroups/2",
    phone: "(646) 450-3064",
    sessionDesc:
      "Interpersonal traumatic events such as sexual assault, rape, abuse from childhood, domestic or intimate partner violence, or any other experience that elicits feelings of extreme fear, shame, guilt, and/or loss of safety can cause an array of symptoms that disrupt your daily life. Often times, being a survivor of trauma can be an isolating experience, causing you to feel alone and disconnected from others who just don’t get it. This virtual support group aims to decrease feelings of isolation and provide you with a nonjudgmental and supportive space to heal in the aftermath of interpersonal trauma.",
    meetingTime: "Every Tue 6:30 P.M. - 8 P.M.",
    website: "https://www.northeastpsychological.com/",
  },
  /* Instaance 3 */
  {
    sessionHostName: "Gabrielle Coolidge",
    location: "Austin, TX",
    address: "706 West Ben White Boulevard B220 Austin, TX 78704",
    sessionName: "Phoenix Rising",
    price: "$",
    traumaIssue: ["Divorce", "Trauma and PTSD", "Women's Issues"],
    image: GabrielleImg,
    link: "/SupportGroups/3",
    phone: "(512) 955-5847",
    sessionDesc:
      "Process group for individuals who are survivors of Narcissistic Abuse. This group is for individuals who are seeking support from people who 'get it', and have a safe space to process through the trauma of being in a narcissistic relationship. This group helps its members identify the 'why' of finding themselves in such a relationship and how to protect themselves from entering another one in the future.",
    meetingTime: "Every Tue 6 P.M. - 7:30 P.M.",
    website: "N/A",
  },

  {
    sessionHostName: "Jhana Rice",
    location: "Austin, TX",
    address: "5900 Balcones Dr Suite 242 Austin, TX 78731",
    sessionName: "Adult Trauma Focused CPT Group",
    price: "$$",
    traumaIssue: ["Anxiety", "Depression"],
    image: JhanaImg,
    link: "/SupportGroups/1",
    phone: "(737) 299-8767",
    email: "hello@austinanxiety.com",
    sessionDesc:
      "This group will use Cognitive Processing Therapy (CPT) to address the impact of trauma on safety, trust, power/control, esteem, and intimacy. In 6-8 weeks, we aim to: improve understanding of PTSD, reduce distress about memories of the trauma, decrease emotional numbing (i.e., difficulty feeling feelings) and avoidance of trauma reminders, reduce feelings of being tense or “on edge”, and decrease depression, anxiety, guilt or shame to improve day-to-day living. Call 512-246-7225 or email hello@austinanxiety.com to join our next group in January 2024.",
    meetingTime: "Not displayed",
    website: "https://www.austinanxiety.com/",
  },
  /* Instance 2 */
  {
    sessionHostName: "Lauren Tepfer",
    location: "Austin, TX",
    address: "Virtual",
    sessionName: "Virtual Support Group- Interpersonal Trauma/Abuse",
    price: "$",
    traumaIssue: ["Sexual Abuse", "Trauma and PTSD"],
    image: LaurenImg,
    link: "/SupportGroups/2",
    phone: "(646) 450-3064",
    sessionDesc:
      "Interpersonal traumatic events such as sexual assault, rape, abuse from childhood, domestic or intimate partner violence, or any other experience that elicits feelings of extreme fear, shame, guilt, and/or loss of safety can cause an array of symptoms that disrupt your daily life. Often times, being a survivor of trauma can be an isolating experience, causing you to feel alone and disconnected from others who just don’t get it. This virtual support group aims to decrease feelings of isolation and provide you with a nonjudgmental and supportive space to heal in the aftermath of interpersonal trauma.",
    meetingTime: "Every Tue 6:30 P.M. - 8 P.M.",
    website: "https://www.northeastpsychological.com/",
  },
  /* Instaance 3 */
  {
    sessionHostName: "Gabrielle Coolidge",
    location: "Austin, TX",
    address: "706 West Ben White Boulevard B220 Austin, TX 78704",
    sessionName: "Phoenix Rising",
    price: "$",
    traumaIssue: ["Divorce", "Trauma and PTSD", "Women's Issues"],
    image: GabrielleImg,
    link: "/SupportGroups/3",
    phone: "(512) 955-5847",
    sessionDesc:
      "Process group for individuals who are survivors of Narcissistic Abuse. This group is for individuals who are seeking support from people who 'get it', and have a safe space to process through the trauma of being in a narcissistic relationship. This group helps its members identify the 'why' of finding themselves in such a relationship and how to protect themselves from entering another one in the future.",
    meetingTime: "Every Tue 6 P.M. - 7:30 P.M.",
    website: "N/A",
  },
];

export { therapistData, facilityData, supportData };

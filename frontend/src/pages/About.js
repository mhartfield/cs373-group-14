import TeamCard from "../components/TeamCard"
import { useEffect, useState } from "react";
import axios from 'axios';
import JamesImg from '../images/contributors/JamesYou.jpg';
import LucyImg from '../images/contributors/LucyXu.jpg';
import MaxImg from '../images/contributors/MaxHartfield.jpg';
import SierraImg from '../images/contributors/SierraPatton.jpg';
import CooperImg from '../images/contributors/CooperWilk.jpg';
import awsImg from "../images/tools/awsamplify.jpg";
import gitlabImg from "../images/tools/gitlab.jpg";
import reactImg from "../images/tools/react.jpg";
import postmanImg from "../images/tools/postman.jpg";
import googleMapsImg from "../images/tools/googlemaps.jpg";
import dockerImg from "../images/tools/docker.jpg";
import flaskImg from "../images/tools/flask.jpg";
import postgreSQLImg from "../images/tools/postgreSQL.jpg";
import puppeteerImg from "../images/tools/puppeteer.jpg";
import pythonImg from "../images/tools/python.jpg";
import yelpImg from "../images/tools/yelp.jpg";
import ToolCard from "../components/ToolCard";
import { fetchContributorData, fetchIssueData } from "../api";
import './About.css';


function About() {
    /* Creates About page, with data on all teammates displayed. */
    // const [contributors, setContributors] = useState();
    const [issues, setIssues] = useState();
    const [results, setResults] = useState();
    const projectId = process.env.REACT_APP_GITLAB_PROJECT_ID;
    const contributors = [
        {
            name: ['loogey'],
            fullName: 'Lucy Xu',
            bio: 'Hi! I\'m a third-year CS student and I\'m interested in ML/AI and full-stack development. I\'m a huge cat person and enjoy playing mobile games in my free time.',
            role: ['frontend'],
            img: LucyImg,
            commits: 0,
            issues: 0,
            unittests: 10
        },
        {
            name: ['James You', 'colmagi'],
            fullName: 'James You',
            bio: 'Hi! I\'m a CS major in my junior year at UT Austin. I enjoy playing the guitar, playing video games, and drawing in my spare time.',
            role: ['frontend'],
            img: JamesImg,
            commits: 0,
            issues: 0,
            unittests: 0
        },
        {
            name: ['maxhartfield', 'Max Hartfield'],
            fullName: 'Max Hartfield',
            emails: [],
            bio: 'Hi! I\m a second-year CS student with interests in software engineering. I like to play video games, read, and workout in my free time.',
            role: ['frontend'],
            img: MaxImg,
            commits: 0,
            issues: 0,
            unittests: 6
        },
        {
            name: 'CenterKeyCooper',
            fullName: 'Cooper Wilk',
            bio: "I'm a third-year CS student with interests in Graphics and ML. I love making games of all varieties and spend a lot of my free time working on producing and hosting Survivor Texas!",
            role: ['frontend'],
            img: CooperImg,
            commits: 0,
            issues: 0,
            unittests: 0
        },
        {
            name: ['SIERRA PATTON', 'SierraP414'],
            fullName: 'Sierra Patton',
            bio: "Hello! I am a third-year CS student at the University of Texas at Austin. Primarily my interests revolve around machine learning, gardening, and weight lifting.",
            role: ['frontend'],
            img: SierraImg,
            commits: 0,
            issues: 0,
            unittests: 0
        }

    ];
    const [c, setC] = useState();

    
    /* Scrapes our contribution from gitlab 
    https://docs.gitlab.com/ee/api/repositories.html */
    useEffect(() => {
        const fetchData = async () => {
            try {
                const [fetchC, fetchI] = await Promise.all([
                    fetchContributorData(projectId),
                    fetchIssueData(projectId)
                ]);
                setResults(fetchC);
                setIssues(fetchI);
            } catch (error) {
                console.log("Error fetching Gitlab API: ", error)
            }
        }
        fetchData();
        if (results){
            results.map(cc => {
                console.log("contributor", cc);
                 const index = contributors.findIndex(contributor => contributor.name.includes(cc.name));
                contributors[index].commits += cc.commits;
            })
        setC(contributors);
        }
    }, [results]);

    /* Displays all utilized tools throughout all Phases, 
       and includes an icon for each. */
    const tools = [
        {
            toolName: 'AWS Amplify',
            img: awsImg
        },
        {
            toolName: 'Gitlab',
            img: gitlabImg
        },
        {
            toolName: 'React',
            img: reactImg
        },
        {
            toolName: 'Postman',
            img: postmanImg
        },
        {
            toolName: 'Google Maps API',
            img: googleMapsImg
        },
        {
            toolName: 'Docker',
            img: dockerImg
        },
        {
            toolName: 'PostgreSQL',
            img: postgreSQLImg
        },
        {
            toolName: 'Python',
            img: pythonImg
        },
        {
            toolName: 'Yelp',
            img: yelpImg
        },
        {
            toolName: 'Flask',
            img: flaskImg
        },
        {
            toolName: 'Puppeteer',
            img: puppeteerImg
        },
    ]

    return (
        <div class="container" style={{ textAlign: "center" }}>
            <div class="row">
                <h1>About Us</h1>
                <p class="about-desc">
                Our platform's purpose is to aid trauma victims in Texas by facilitating connections to vital resources and support networks. We aim to provide trauma victims easy access to essential information and services, including counseling, support groups, and trauma facilities.
By centralizing these resources, we can empower trauma survivors on their journey toward healing and recovery.
<br></br><br></br>
The process of integrating disparate data made us think critically about how we could connect information from one source to another. The multiple sources confirmed how costly seeking help for trauma can also be.
                </p>
            </div>

            <div class="row">
                <h1>Meet the Team</h1>
                <div class="row team-container">
                    {
                        c && issues ? c.map((contributor) => {
                            // console.log("contributor", contributor);
                            const numIssues = issues.filter(issue => contributor.name.includes(issue.closed_by.name)).length;
                            // const extra = additionalInfo.find(additionalInfo => additionalInfo.name === contributor.name);
                            // console.log("extra:", extra);
                            return (
                                <div class="col">
                                    <TeamCard
                                        name={contributor.fullName}
                                        commits={contributor.commits}
                                        issues={numIssues}
                                        moreInfo={contributor}
                                    >
                                    </TeamCard>
                                </div>
                            );
                        }) : <h2>Loading...</h2>
                    }

                </div>
            </div>
            <div class="row">
                <h1>Project Tools</h1>
                <div className="tools-container">
                    {tools.map((tool) => (
                        <ToolCard toolName={tool.toolName} img={tool.img}></ToolCard>
                    ))}
                </div>
            </div>
            <div class="row data-sources-container">
                <h1>Data Sources + Postman API Documentation</h1>
                <h3><a href="https://www.dshs.texas.gov/dshs-ems-trauma-systems/trauma-system-development/texas-trauma-facilities">Texas Health and Human Services</a></h3>
                <h3><a href="https://www.psychologytoday.com/us/groups/texas?category=trauma-focused">Psychology Today Support Groups</a></h3>
                <h3><a href="https://meetmonarch.com/therapists?location=Texas&specialty=Trauma">MeetMonarch Trauma Therapist Directory</a></h3>
                <div class="row mt-5">
                <h3><a href="https://documenter.getpostman.com/view/32907588/2sA2r53k1z">Postman API Documentation</a></h3>
                </div>
            </div>
        </div>
    )
}
export default About;
import React from "react"
import TraumaFacilityCard from "../../components/TraumaFacilitiesCard";
import "./Model.css";
import { useEffect, useState } from "react";
import { fetchFacilitiesData } from "../../api";

const FacilityModel = () => {

  const supportsPerPage = 9;
  const totalPages =  34; //Math.floor(301 + 8 / 9);
  const [currentPage, setCurrentPage] = useState(1);

  const handlePageChange = (page) => {
     setCurrentPage(page);
  };


  /* Creates model and sets up card with 
     all attributes in database. */
     const [results, setResults] = useState();
     useEffect(() => {
      const fetchData = async () => {
          try {
              const result = await fetchFacilitiesData(currentPage);
              setResults(result);
              console.log("RES", result);
          }
          catch (e) {
              console.log("Error fetching API:", e)
          }
      }
      fetchData();
  }, [currentPage]);

  return (
    <div class="container">
      <h1 class="model-header">Trauma Facilities in Texas</h1>

      <div className="filters">
        <label>Level:</label>
        <select        >
          <option value="">All</option>
          <option value="Comprehensive">Comprehensive</option>
          <option value="Not Comprehensive">Not Comprehensive</option>
          {/* add options dynamically based on data */}
        </select>

        <label>Trauma Support Area:</label>
        <select
        >
          <option value="">All</option>
          <option value="L">L</option>
          <option value="E">E</option>
          <option value="P">P</option>
          <option value="O">O</option>
          {/* add options dynamically based on data */}
        </select>

        <label>City:</label>
        <select
        >
          <option value="">All</option>
          <option value="">Dallas</option>
          <option value="">Austin</option>
          <option value="">Houston</option>
          <option value="">Other</option>
          {/* add options dynamically based on data */}
        </select>

        <label>Yelp Review:</label>
        <select
        >
          <option value="">All</option>
          <option value="African American">0-1</option>
          <option value="Caucasian">1-2</option>
          <option value="Asian">2-3</option>
          <option value="Hispanic">3-4</option>
          <option value="Native American">4-5</option>
          {/* add options dynamically based on data */}
        </select>

        <label>Price Range:</label>
        <select
        >
          <option value="">All</option>
          <option value="">$</option>
          <option value="">$$</option>
          <option value="">$$$</option>
          {/* add options dynamically based on data */}
        </select>
        
      </div>


      <div className="row card-container">
        {results ? results.map((facility, index) => {
          return (
            <div class="col" data-testid="facility-card">
              <TraumaFacilityCard key={index} {...facility} />
            </div>
          );
        }) : "Loading..."}
      </div>

      <div className="pagination">
        <button
          key={currentPage - supportsPerPage}
          onClick={() => handlePageChange(currentPage - supportsPerPage)}
          className={currentPage === currentPage - supportsPerPage ? "active" : ""}
          disabled={currentPage <= supportsPerPage}
        >
          {"<<"}
        </button>

        <button
          key={currentPage - 1}
          onClick={() => handlePageChange(currentPage - 1)}
          className={currentPage === currentPage - 1 ? "active" : ""}
          disabled={currentPage === 1}
        >
          {"<"}
        </button>

        {Array.from({ length: Math.min(totalPages - Math.floor((currentPage - 1) / supportsPerPage) * supportsPerPage, supportsPerPage) }, (_, index) => {
          const batch = Math.floor((currentPage - 1) / supportsPerPage) * supportsPerPage;
          const pageNumber =  batch + index + 1;
          const isCurrent = currentPage === pageNumber;
          index += currentPage

          return (
            <button
              key={pageNumber}
              onClick={() => handlePageChange(pageNumber)}
              className={isCurrent ? "active" : ""}
            >
              {pageNumber}
            </button>
          );
        })}

          <button
            key={currentPage + 1}
            onClick={() => handlePageChange(currentPage + 1)}
            className={currentPage === currentPage + 1 ? "active" : ""}
            disabled={currentPage === totalPages}
          >
            {">"}
          </button>
          <button
            key={currentPage + supportsPerPage}
            onClick={() => handlePageChange(currentPage + supportsPerPage)}
            className={currentPage === currentPage + 1 ? "active" : ""}
            disabled={currentPage >= totalPages - supportsPerPage}
          >
            {">>"}
          </button>
          
          

      </div>
      <div className = "pages">
        <p>{currentPage} of {totalPages} pages</p>
      </div>


    </div>
  );
};

export default FacilityModel;

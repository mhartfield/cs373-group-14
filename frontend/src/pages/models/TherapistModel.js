import React, { useState, useEffect } from "react";
import TherapistCard from "../../components/TherapistCard";
import "./Model.css";
import { therapistData } from "../data";
import { fetchTherapistsData } from "../../api"

const TherapistModel = () => {

  //Page
  const therapistsPerPage = 9;
  const totalPages = Math.floor(410 / 9);
  const [currentPage, setCurrentPage] = useState(1);
  const therapists = therapistData;

  

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  // const visibleTherapists = therapists.slice(
  //   (currentPage - 1) * therapistsPerPage,
  //   currentPage * therapistsPerPage
  // );

  
  const [searchQuery, setSearchQuery] = useState("");

  const filteredTherapists = therapists.filter((therapist) =>
    therapist.name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  // Rendering Page Buttons

  //Check Boxes

  const [selectedInsurance, setSelectedInsurance] = useState("");
  const [selectedSpecialties, setSelectedSpecialties] = useState("");
  
  const applyFilters = (therapists) => {
    return therapists
      .filter((therapist) =>
        therapist.name.toLowerCase().includes(searchQuery.toLowerCase())
      )
      .filter(
        (therapist) =>
          selectedInsurance === "" || therapist.insurance === selectedInsurance
      )
      .filter(
        (therapist) =>
          selectedSpecialties === "" ||
          therapist.specialties.includes(selectedSpecialties)
      );
    // filtering logic for other criteria as needed
    // ...
  };


  // load therapists when the component mounts
  const [results, setResults] = useState();

     useEffect(() => {
      const fetchData = async () => {
          try {
            const therapistsPromise = await fetchTherapistsData(currentPage);
           
            console.log("THERAPIST PROMISE", therapistsPromise);
            setResults(therapistsPromise);
          }
          catch (e) {
              console.log("Error fetching API:", e)
          }
      }
      fetchData();
  }, [currentPage]);

  return (
    <div className="container">
      <h1 class="model-header">Trauma Therapists in Texas</h1>

      <div className="filters">
        <label>Insurance:</label>
        <select
          value={selectedInsurance}
          onChange={(e) => setSelectedInsurance(e.target.value)}
        >
          <option value="">All</option>
          <option value="Accepted">Accepted</option>
          <option value="Not Accpeted">Not Accepted</option>
          {/* add options dynamically based on data */}
        </select>

        <label>Specialties:</label>
        <select
          value={selectedSpecialties}
          onChange={(e) => setSelectedSpecialties(e.target.value)}
        >
          <option value="">All</option>
          {/* add options dynamically based on data */}
        </select>

        <label>Target Age Group:</label>
        <select
          value={selectedSpecialties}
          onChange={(e) => setSelectedSpecialties(e.target.value)}
          //change method for this
        >
          <option value="">All</option>
          <option value="">Youth</option>
          <option value="">Adult</option>
          <option value="">Elderly</option>
          {/* add options dynamically based on data */}
        </select>

        <label>Race & Ethnicity:</label>
        <select
          value={selectedSpecialties}
          onChange={(e) => setSelectedSpecialties(e.target.value)}
          //change method for this
        >
          <option value="">All</option>
          <option value="African American">African American</option>
          <option value="Caucasian">Caucasian</option>
          <option value="Asian">Asian</option>
          <option value="Hispanic">Hispanic</option>
          <option value="Native American">Native American</option>
          {/* add options dynamically based on data */}
        </select>

        <label>Price Range:</label>
        <select
          value={selectedSpecialties}
          onChange={(e) => setSelectedSpecialties(e.target.value)}
          //change method for this
        >
          <option value="">All</option>
          <option value="">$</option>
          <option value="">$$</option>
          <option value="">$$$</option>
          {/* add options dynamically based on data */}
        </select>
        
      </div>
      

      <div className="row card-container">
        {results ? results.map((therapist, index) => {
          return (
            <div key={index} class="col" data-testid="therapists-card">
              <TherapistCard {...therapist} />
            </div>
          )
        }): "Loading..." }
      </div>
      <div className="pagination">

      <button
        key={currentPage - therapistsPerPage}
        onClick={() => handlePageChange(currentPage - therapistsPerPage)}
        className={currentPage === currentPage - therapistsPerPage ? "active" : ""}
        disabled={currentPage <= therapistsPerPage}
      >
        {"<<"}
      </button>

      <button
        key={currentPage - 1}
        onClick={() => handlePageChange(currentPage - 1)}
        className={currentPage === currentPage - 1 ? "active" : ""}
        disabled={currentPage === 1}
      >
        {"<"}
      </button>

      {Array.from({ length: Math.min(totalPages - Math.floor((currentPage - 1) / therapistsPerPage) * therapistsPerPage, therapistsPerPage) }, (_, index) => {
        const batch = Math.floor((currentPage - 1) / therapistsPerPage) * therapistsPerPage;
        const pageNumber =  batch + index + 1;
        const isCurrent = currentPage === pageNumber;
        index += currentPage

        return (
          <button
            key={pageNumber}
            onClick={() => handlePageChange(pageNumber)}
            className={isCurrent ? "active" : ""}
          >
            {pageNumber}
          </button>
        );
      })}

      <button
        key={currentPage + 1}
        onClick={() => handlePageChange(currentPage + 1)}
        className={currentPage === currentPage + 1 ? "active" : ""}
        disabled={currentPage === totalPages}
      >
        {">"}
      </button>
      <button
        key={currentPage + therapistsPerPage}
        onClick={() => handlePageChange(currentPage + therapistsPerPage)}
        className={currentPage === currentPage + 1 ? "active" : ""}
        disabled={currentPage >= totalPages - therapistsPerPage}
      >
        {">>"}
      </button>

      </div>
      <div className = "pages">
        <p>{currentPage} of {totalPages} pages</p>
      </div>
    </div>
  );
};

export default TherapistModel;

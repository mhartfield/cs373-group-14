import TraumaSupportGroupsCard from "../../components/TraumaSupportGroupsCard";
import './Model.css';
import { useEffect, useState } from "react";
import { fetchSupportGroupsData } from "../../api";

const SupportGroupModel = () => {
  /* Creates model and sets up card with 
     all attributes in database. */

     const supportsPerPage = 9;
     const totalPages = 317 ;//Math.floor(2851 + 8 / 9);
     const [currentPage, setCurrentPage] = useState(1);

     const handlePageChange = (page) => {
        setCurrentPage(page);
     };

     const [results, setResults] = useState();
     useEffect(() => {
      const fetchData = async () => {
        try {
            const result = await fetchSupportGroupsData(currentPage);
            setResults(result);
        }
        catch (e) {
            console.log("Error fetching API:", e)
        }
    }
    fetchData();
  }, [currentPage]);

  return (


    <div class="container">
      <h1 class="model-header">Trauma Support Group Leads in Texas</h1>

      <div className="filters">
        <label>Insurance:</label>
        <select        >
          <option value="">All</option>
          <option value="Accepted">Accepted</option>
          <option value="Not Accpeted">Not Accepted</option>
          {/* add options dynamically based on data */}
        </select>

        <label>Trauma Focus:</label>
        <select
        >
          <option value="">All</option>
          {/* add options dynamically based on data */}
        </select>

        <label>Target Age Group:</label>
        <select
        >
          <option value="">All</option>
          <option value="">Youth</option>
          <option value="">Adult</option>
          <option value="">Elderly</option>
          {/* add options dynamically based on data */}
        </select>

        <label>City:</label>
        <select
        >
          <option value="">All</option>
          <option value="">Dallas</option>
          <option value="">Austin</option>
          <option value="">Houston</option>
          <option value="">Other</option>
          {/* add options dynamically based on data */}
        </select>

        <label>Price Range:</label>
        <select
        >
          <option value="">All</option>
          <option value="">$</option>
          <option value="">$$</option>
          <option value="">$$$</option>
          {/* add options dynamically based on data */}
        </select>
        
      </div>


      <div class="row card-container">
        {results ? results.map((support, index) => {
          return (
          <div class="col" data-testid="support-group-card">
            <TraumaSupportGroupsCard key={index} {...support} />
          </div>
          )
        }): "Loading..."}
      </div>


      <div className="pagination">
        <button
          key={currentPage - supportsPerPage}
          onClick={() => handlePageChange(currentPage - supportsPerPage)}
          className={currentPage === currentPage - supportsPerPage ? "active" : ""}
          disabled={currentPage <= supportsPerPage}
        >
          {"<<"}
        </button>

        <button
          key={currentPage - 1}
          onClick={() => handlePageChange(currentPage - 1)}
          className={currentPage === currentPage - 1 ? "active" : ""}
          disabled={currentPage === 1}
        >
          {"<"}
        </button>

        {Array.from({ length: Math.min(totalPages - Math.floor((currentPage - 1) / supportsPerPage) * supportsPerPage, supportsPerPage) }, (_, index) => {
          const batch = Math.floor((currentPage - 1) / supportsPerPage) * supportsPerPage;
          const pageNumber =  batch + index + 1;
          const isCurrent = currentPage === pageNumber;
          index += currentPage

          return (
            <button
              key={pageNumber}
              onClick={() => handlePageChange(pageNumber)}
              className={isCurrent ? "active" : ""}
            >
              {pageNumber}
            </button>
          );
        })}

          <button
            key={currentPage + 1}
            onClick={() => handlePageChange(currentPage + 1)}
            className={currentPage === currentPage + 1 ? "active" : ""}
            disabled={currentPage === totalPages}
          >
            {">"}
          </button>
          <button
            key={currentPage + supportsPerPage}
            onClick={() => handlePageChange(currentPage + supportsPerPage)}
            className={currentPage === currentPage + 1 ? "active" : ""}
            disabled={currentPage >= totalPages - supportsPerPage}
          >
            {">>"}
          </button>

      </div>
      <div className = "pages">
        <p>{currentPage} of {totalPages} pages</p>
      </div>
    </div>
  );

};

export default SupportGroupModel;

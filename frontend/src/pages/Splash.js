import Slideshow from "../components/Slideshow";
import LiteYouTubeEmbed from "react-lite-youtube-embed";
import "react-lite-youtube-embed/dist/LiteYouTubeEmbed.css";
import { Link } from "react-router-dom";
import "./Splash.css";

const aboutText =
  "Our platform is dedicated to aiding trauma victims in Texas by facilitating connections to vital resources and support networks. Our goal is to provide easy access to essential information and services, including counseling, support groups, and trauma facilities. By centralizing these resources, we hope to empower trauma survivors on their journey toward healing and recovery.";
function Splash() {
  /* Returns Splash Page including description of our mission, 
     types of traumas, number of instances per model, interview 
     with survivor, donation link, and self care tips for trauma 
     victims for user stories. */
  return (
    <div style={{ backgroundColor: "#DCF2FF" }}>
      <div className="landing">
        <div>
          <h1 styles="font-size: 55px;" className="splashtext maintext">
            Texas Trauma Support
          </h1>
          <h4 className="splashtext">
            You are never alone. Help is only a click away.
          </h4>
        </div>
      </div>
      <div className="container text-center d-flex justify-content-center splash-row-container">
        <div className="row">
          <div className="col columntext">
            <h2 className="pt-2">
              <div className="badge bg-secondary d-grid gap-2">Our Mission</div>
            </h2>
            <p className="mission-text">{aboutText}</p>
            <h2 className="pt-2">
              <div className="badge bg-secondary d-grid gap-2">
                What We Provide
              </div>
            </h2>
            <Link className="nav-link d-grid gap-2" to="/Therapists">
              <button className="btn btn-outline-primary btn-lg">
                410 Therapists
              </button>
            </Link>
            <Link className="nav-link d-grid gap-2" to="/Facilities">
              <button className="btn btn-outline-primary btn-lg">
                301 Trauma Facilities
              </button>
            </Link>
            <Link className="nav-link d-grid gap-2" to="/SupportGroups">
              <button className="btn btn-outline-primary btn-lg">
                2851 Trauma Support Groups
              </button>
            </Link>
            <h2 className="pt-2">
              <div className="badge bg-secondary d-grid gap-2">Self Care</div>
            </h2>
            <ul class="list-group list-group-flush">
              <li class="list-group-item">Get More Rest</li>
              <li class="list-group-item">Find Someone to Talk To</li>
              <li class="list-group-item">Exercise</li>
              <li class="list-group-item">Find Engaging Hobbies</li>
              <li class="list-group-item">Meditate</li>
              <li class="list-group-item">Listen to Music</li>
            </ul>
          </div>
          <div className="col">
            <h2 className="pt-2">
              <div className="badge bg-secondary d-grid gap-2">
                Types of Trauma
              </div>
            </h2>
            <Slideshow />
            <div>
              <h2 className="pt-2">
                <div className="badge bg-secondary d-grid gap-2">
                  A Survivor's Story
                </div>
              </h2>
              <div style={{ borderRadius: "0.5rem", overflow: "hidden" }}>
                <LiteYouTubeEmbed
                  id="05-S-s-DdBY"
                  title="Trauma Survivor: Brian Worster"
                />
              </div>
            </div>
          </div>
        </div>
        <div className="row donate-section">
          <h2 className="pt-2">
            <div className="badge bg-secondary d-grid gap-2">Donate Here</div>
          </h2>
          <div className="d-grid gap-2 d-md-block ">
            <a
              href="https://give.rainn.org/a/donate"
              className="btn btn-outline-info mx-2"
              role="button"
              target="_blank"
              rel="noreferrer"
            >
              RAINN
            </a>
            <a
              href="https://www.thetrevorproject.org/"
              className="btn btn-outline-info mx-2"
              role="button"
              target="_blank"
              rel="noreferrer"
            >
              theTrevorProject
            </a>
            <a
              href="https://www.traumasupportservices.org/"
              className="btn btn-outline-info mx-2"
              role="button"
              target="_blank"
              rel="noreferrer"
            >
              traumasupportservices
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Splash;

/**
 *             <div className="row p-2">Want to help? Donate to these programs.</div>
            <div className="row p-2">            
              <button type="button" className="btn btn-outline-info">RAINN</button>
            </div>
            <div className="row p-2">
              <button type="button" className="btn btn-outline-info">theTrevorProject</button>   
            </div>
            <div className="row p-2">   
              <button type="button" className="btn btn-outline-info">traumasupportservices</button>
            </div>
 */


import { render, screen, act, fireEvent  } from '@testing-library/react';
import TherapistCard from '../components/TherapistCard';
import TraumaSupportGroupsCard from '../components/TraumaSupportGroupsCard';
import TraumaFacilityCard from '../components/TraumaFacilitiesCard';
import TherapyInstance from '../pages/instances/TherapistInstance';
import SupportGroupInstance from '../pages/instances/SupportGroupInstance';
import FacilityInstance from '../pages/instances/FacilityInstance';
import '@testing-library/jest-dom';
import { fetchTherapistData, fetchSupportGroupData, fetchFacilityData, fetchTherapistsData, fetchFacilitiesData, fetchSupportGroupsData, fetchContributorData, fetchIssueData } from '../api';
import { MemoryRouter } from 'react-router-dom'; 
import Splash from '../pages/Splash.js';
import About from '../pages/About.js';
import FacilityModel from '../pages/models/FacilityModel.js';
import TherapistModel from '../pages/models/TherapistModel.js';
import SupportGroupModel from '../pages/models/SupportGroupModel.js';


jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: jest.fn().mockReturnValue({ id: 1 }),
}));

const mockTherapistData = {"answers":["Wendy Aporta is currently accepting new clients and can be booked on the Monarch website.","Wendy Aporta does offer telehealth appointments. You can request to book a telehealth appointment with them on their Monarch profile.","Wendy Aporta specializes in the following areas: Abuse Survivors, Anxiety, Depression, Dissociative Identity Disorder (DID), Domestic Violence, Family Conflict, Marital and Premarital, Military/Veteran's Issues, PTSD, and Trauma."],"gender":"Female","id":"000e2b14-5f0f-4e14-bccf-a1bc715eef70","image":"https://d2c3hp2382bqvj.cloudfront.net/uploads/clinician/profile_img_file/396428/thumb_open-uri20210524-6655-58og2g","insurance":true,"intro":"Ms. Wendy Aporta is a Licensed Marriage and Family Therapist and Certified Clinical Trauma Professional in the State of Texas. She obtained her Master's degree in psychology from Chapman University in and is currently pursuing a PhD in Marriage and Family Therapy with a specialty in Military Families from North Central University.\nMs. Aporta cut her teeth as a certified domestic violence counselor as an intern, moved to mobile crisis response and suicide hotline as an LMFT-associate, and then moved into full time trauma work as a fully licensed LMFT. For Ms. Aporta, helping those with traumatic experience is not just a job, it is a vocation. She is a perpetual student of trauma, constantly staying abreast of the latest research and treatments to provide the best possible options and outcomes for her clients. Her most frequent modalities are Internal Family Systems and Acceptance and Commitment Therapies, Somatic work, and biofeedback. However, she is experienced in other modalities, including CBT, DBT, and SFBT, and utilizes whatever is best for her client's needs. She is proficient with clients experiencing depression, anxiety, PTSD, and dissociative identity disorder.","location":"Plano","name":"Wendy Aporta","pricingPrices":[],"pricingService":[],"questions":["Is Wendy Aporta accepting new clients and do they offer online appointment requests?","Does Wendy Aporta offer telehealth appointments?","What areas does Wendy Aporta specialize in?"],"raceEthnicity":"White","specialties":["Trauma","PTSD","Military/Veteran's Issues","Abuse Survivors","Dissociative Identity Disorder (DID)","and more"],"targetAgeGroup":["Teenagers (12-18)","Young Adults (18-24)","Adults (24+)","Elders (65+)"]}

const mockTherapistsData = [mockTherapistData, mockTherapistData, mockTherapistData, mockTherapistData, mockTherapistData, mockTherapistData, mockTherapistData, mockTherapistData, mockTherapistData];

const mockSupportGroupData =  {"address":"4101 Greenbriar Dr. 4101 Greenbriar Dr Suite 122 F Houston, TX 77098","id":"4c199046-cdac-4135-b74c-f5f516280b1d","image":"https://photos.psychologytoday.com/3e5f2ff6-46cd-11ea-a6ad-06142c356176/2/320x400.jpeg","location":"Houston","meetingTime":"Not specified","phone":"(832) 924-3658","price":0,"sessionDesc":"The approach is educational, skilltraining model designed to improve satisfaction and stability in the couple and other types of relationships. Couples learn how to be more accepting and nonjudgmental of each other; to improve ownership and regulation of their emotional expression; to become more empathic and skillful in conveying empathy and understanding to each other; and to strengthen the emotional engagement (attachment) in their relationship. Other relational issues: Through discussions, individuals gain an insight into the situations and circumstances that have been disabling them from building successful relationships w/ others. 3-8 people required to start a group.","sessionHostName":"Ivana Radovancevic","sessionName":"Relationship & Social networking","traumaIssue":["Eating Disorders","Peer Relationships","Relationship Issues"],"website":"https://www.houstononeclickcounseling.com/"}

const mockSupportGroupsData = [mockSupportGroupData, mockSupportGroupData, mockSupportGroupData, mockSupportGroupData, mockSupportGroupData, mockSupportGroupData, mockSupportGroupData, mockSupportGroupData, mockSupportGroupData];

const mockFacilityData = {
  "facilityName": "Baylor Scott & White Medical Center - Temple",
  "city": "Temple",
  "address": "2401 S 31st St, Temple, TX 76508",
  "level": "Comprehensive",
  "traumaSupportArea": "L",
  "zipCode": 76508,
  "image": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBIVFRgVFRUYGBgYGBgYGBgaGBgYGBgYGBgZGhgYGBgcIS4lHB4rIRgYJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHRISHjQrISE0NDQ0NDQ0MTQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ1NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIAMIBAwMBIgACEQEDEQH/xAAbAAADAQEBAQEAAAAAAAAAAAAAAwQBAgUGB//EAD4QAAEDAgMEBwUIAQMFAQAAAAEAAhIDEQQhMQUTQVEiUmGBkaHRBmJxkrEUFSMyQrLB4YJTcvAWM0NE8Qf/xAAZAQEBAQEBAQAAAAAAAAAAAAAAAQIDBAX/xAAlEQEAAgICAQQCAwEAAAAAAAAAARECEgMxIRNBUYEioQRhkRT/2gAMAwEAAhEDEQA/APmIrIqg0yuIr775ZMURTw1bu0VPFEVS2ldcliBEURTooiqlkxRFOiiKFkxRFOissiWVFEU6KIoWTFEU2yLIWVFEU2yLIWVFEU2yLItlRRFNsiyhZUURTIoihZUURTYoii2VFEUyKIoWVZFkyKIqLZdlkU2KyKg9vZbfwm9/7isXezR+G3v/AHFCyrzg0rC1ODVpaF0c7TxXTWpoatLES3JaLLghMiiKFlFqyydFbEIWRBFk+CIKlp4Ignhq6LVEtNBBYqCxcxVLJgsinxRFCyIoiqIrIoWRFEU+KIoWRFEU+KIqFkRWFqoisgi2TFZFURWQQsiKIJ8EQRbILFkU+KyKFkxWFqfFZBRbers1v4be/wDcUJ2zW/ht7/3FYsNoA1BYmBqY1l10cU8URToLIqhUUFqe0DkiygRFEU2IRFUKigtTS1ZFELiiKbBZFAuKIpsURRHFguYpxYsigTFEU2KIoFRRFNiiKBUURTYoiikxRFOiiKgTFEU6KyyKTFEU6yLIExWRT7LIoEwWWT4rC1Rp6ez2/ht7/qUJmz2/ht7/AKlC5tJXYdw4LiBXqCoOKY2J4BXafeF1iepeXTiDndNc8dpVVekOSmDe5WKnyzNx4IfbkuSzkrN0Of0XD2LUSzMJTTWtBGicWrILTJZN9b3WAJsFoaoEkILU6KyCqFtyWEJ0EQQIgiKfBZBEJiiCdBEEUiCIJ8EQQIgiCfBYWIERRFPgiCKRBEU6KIqBMFkU+KyKKSWrIp8VkUCYrIp0UFijT0dnt/Db3/UrVuDb0B3/AFKFhSt2QtaSE43WALQzelcPdfgu4IgkRCTMlNy+KCOabBEFWSS3sWQT4LYIJ4LYJ+7WwSyk8EQVEERS0pPBEFRBbBLKTQRBUwRBLKTQRBUQRBLKTwRBURRBLKTQRBUQRBLWk8FkFTBZBLKTwWRVO7RBLKTRRFU7tZu1LWk8VkFRFEUspNFBYqILCxS2qV4NvQHf9ShOwjegO/6lYsqXBEFTFbBW0SwRBVQRBLRLBEFVBEFbEsFsFTBEFLZTQWwVMEQVsTwRBU7tG7SxNBEFVu0btLEsEQVW7WbtTYTQRBU7tG7TYTQWRVW7Ru0sSwRFUwRBNhLFEVVBZBLEu7Ru1VBEE2aS7tEFTBEEsSmms3aq3azdqWqWKwsVO7QWJan4RvRHf9ShPwzOiO/6lC5W0IIgqRTW7tXZmk27WQVW7WFlsyrsUmgt3aewA6EH4G663abJSbdo3aqgiCbFJhTW7tUhi2CmxSXdrYKvdo3am5qkgiCr3aN2m5qkgiCqgiCbGqTdo3argiCbGqQ00qhRIvdxcbnW30GQV+6XFNmv+4/wputJ92jdqvdIgrsapN2jdqqCIJsUk3aN2q4LIJsUkgsgr8LRDnhpuLi9wL8baL1HbEZbJzvALnlz44zUumPFllFw+bLFkFYaa5LF02YpIaaw01WWLCxTYp1hmdEd/wBStTqDOiO/6oWNlowMXUEyk9rhdrgQeIN0yK5eo6enJEEQXNPGU3PLBe4uNDbK97HuVQYkcsT1K5cOWM1MTCKkwAfEn6lMgu6TegztDfpdGKrsptk4gDIXPaQBp2kKZc0Yxa48M5ZVDAxbu0g41rpNBFw6ByOTiLgaciCvPrsxtEtO6NVp6pLuF87DJeef5czP4x/r0f8AJGMflP8Aj1938PEKTC45rsRuYEkEAnhwKVj8XWpU2P3ObrXa69m3bcjUcclP7M06jsUazmRaQbkHohxYMtclPXyyib8Ll/HwxmKuX1WJoUmNLiDwyB1J0C3DYek9sg08QQSciNRkVTiKLXtif7FtCEUKTWNi3t11JOpK4+pl8y1phXXlFWwtNovn3KQ7v3vIKvaxqCi80wS+3RsLm/wsbr4zEY7HtzeXsH+2N8uHRCRy8k+7Xp8fvFvppU+q75h6LZN6h+Y+i+ZZtWq9hYyZqkdFwPSJ5356rznVMfIhz6gsc+m42y4gOyzVjPPLvKmssOLGqxt9o+sAbbsfC7r/AFTKeKp3saQHeV8vRxDzhrTJqC+cru/PcdK99EvZmKrzbOqYaOm/L8p1u7nZY3zm/wAumtOPx+Pb7JtWhEgDMl3Anieeihpll3a/m5e61efRq1DTNn0ZyOchD82drnPK6XVxLmkObUYBMT/K6Qiy4b1f1KRnnHu16XH8PXJZ2+A9Vhc3t8lPhqNSo8FtQBkR0CzORFwZjPiMkyhs3Eh7y90mE9BsWtiPjqe9b9bliO2fR4Znr9unvaOB8f6WCo3tXFHZmJa1we+btQ6zWxB0yvZIcSHRL2X7S3jpmkfyOSJ7v6P+fhyjqvtY2x0WwXWDwdQm5DbW1Dgf5+Cs+yv5eYXq4+fbH8u3i5eCMcqx8w8baGGBbK7mluYc0kG18xlqF6FSgIDpOyte51+Knx+y6wD3zAYGklvSJ0OQsbW081W43pZ8gfDNc884yyif7dMMZxxmEhYsLFQQuSF6t3m0TliwsTH1WA2Lmg8iQCtkLXvl2ZpuaGUGdELF1QxDYjMcfqhY2a0fmOx/aCtSEWGQbrdp1tkPgLk969ep7Y1sg6nYEAZAkknQg9vYvhhXi0BzQ3XQk24WVuEwtR7iW1LENNnEm4yzHy8ua80cczERf7e6Y1u4/T6B+36jXF1tcwBIEfEcNOK97Ae0DnAyFsuTjwzBsvO2V7FPcxz31wwkAta107iN8w05aL5XF4ltCo5gcXwdlnke21+KnoxMdzCTyz3Vvuq22S0MLLOc1oGhy6NuPxUL9sVGjNl76ZEgG68LY+KlJ5IycbWvkTc27svFPdWc5xJeT4jXs8FyqvDW3u9lu1nugGtLOmLkkDK+Z52svd9oPaI4VrDTtVm0tPTc5rYAZtANgSXeQXw7sQb9ltdVjMfHPQacM+WSmsJOUz2+l2/tJ8Gt3gc4ky6RdHkNbc/BfPja9Vosyo9nOL3NBsOwrk7RvqdLX+Jy8Fy3HhxIyy4EDgrEUTNlM2xjCSd9Wtn+t17cOK+k9ito1H1S+vWqQaDadQwLxbW+tgSbfBT4bCUiwGHPME2PnmqqbKYFgxltbRFvCy3lMTFRCRH9vbb7SGOIIe2TXEUBYdJo0Pb3r5vH7cxNUfiZ20AAH0+KqqMpusIhueZYA0kctFrcHSI1IPAucDn8q51Xs1HdvGwlWo5xgCS0F2h4a6jPj4JFTFPHSJcAc7gHMk5Z6WXvfdzSQRWDCARdpJJuLHKNhqQmYLZFMljW1i50rAEZvjnEXIA0PgrERKZW8F1SpkdMri+RsMtAqaheyo0PaW3aHOsMxIXGRPEeRX1tb2UrPdL7S6mHF1mCmxwYCSQ24NrDRfN7f2c9ta1dr3tLWSfa5LW9G4Lb200W9YhmLeliNuVG06RZTo1HEPNQGm5xZ0jbIG+eXiE2rtlnTDt1D8NzbMDS4OLGvDWl3RIbI2PELz8Js/Bza9tX8KTYh7QS4Xsb3OQvfO2StxWy8C7ojENnYlrWsEZH9MmixzUrH5Ll62xaoJY5jei4tzLhewNh0Qrcfto0qzmPbdsWlsR0pHW5Jtb4BeXsjCxNMizmtcBIBuRve1r3C8r/APQcSWYhoAbnTaQTe4s51yLFKuKgnw9mj7T4UOe2pFjsxk5z2uBzLuiLD4a8F4+2MKx9PeU6rHtLZWB6V+jchuupdkviqr9bOsdWiwcNBqTodSijha72Tc07u8XODbC+tpADNa9L3tPUl9PsDabmUK35TL8nTbNrx7hzibjNZitq4ssc0OgdSQ0Ss7t4a8Oa+XwmHcH3LmNbxNnmxzIblmL2IXbKddzrwBEtWvf0siM+lfRdNK6lmcr7h9pszHCnhqr8Q4b8zpscTJzmAXAH+Rd5L1qW0adTDPg/pNZfMOFiNL3HML8wxmHrAlzgXtuDYuLiMzlf9I9Qq8Ft/EBzRumFrSLX/SOeufHVYniylYziHvu27jHEtLS2wN7hrLWBJzdbgCUhm3K+Zm6zBckjoDiOl+U/ytqbXxdOZZg5sJHTY57rl3Sa6zHXBsb27c+ChZ7a40OLHuo9G5DHsIsRcgOsL8fXit6zKddwlx2LeQXF4Ivpe5zvmBrbVZsza9Rt2te4c2jPy4Lydo7YbiKxfWaxlwA5tNjgHHLpZ8bd2io2Y8GqXMNE3sROQIj+U5EXIV0ntZyiqfVYTaboDpHjw7Sheds/alXdtsxlgLCxcBYEjIF9+CFnWflLhFW9lMW97nvqU3ucbkukLngYhuvDwW0/ZjENN50rk5jpacI3bkvsKWP2aNa7u5jv5aunbU2bwq1D3H0Xlxy5Y6enKccp8vm6WyMY24FSgAebCf4UlL2NcXFz6zSTc5Mcddcriy+t+99nj9bz8yPv7AjQE/EO9Fqcuae2Kwh4uy9hMpUyxzi55eXyaCBmALa9ipZsZvXd8I/2vQd7SYUflaPld6JD/aSjwA+Un6rOuc9r+KVuwJXs93h/AN142P8AZPEF12lkeVwDpbQX+q91/tEw/qPc1Tv2zTPFx7v7WsccoSdXkUvZh/66oF7XDQSctF7lLZuGaD+GXl35nOOZ52zy7kgbYp8neH9rfvenyf4D1WpjKUjWF1JzWMgxga3xPiUCoeSlbten1HeXquvven1HeXqlT8LcLGMceA8VQym39QIXlN2u0aMd4hdffQ6h8QpMZFw9yjhGk3D7dts/JR4v2alctxbgS4uHRPRJ5G/ac15w2yOofFadunqnx/pSsi8UtTZuJa7pVnOaL6Pfc8jfh4FV7HxNSgwsc4PJcXSc0OOdha7gcsuanqbXJ/SfEeimdjr/AKfP+lupmPKXEdPQxDKb3F83NJt+lpGXCwK1uEpXBZWIIIIuy1iM73Xmfbvd8/6WHG+75/0s6Ls+s2ZXqsEZh7JB1ibuB7D6rj2lw78S5rw0tLWgWtK8XEjO4tqvmG4/s8/6T27Yc3SQ/wAk1n2LhuP2HINIk12Ursu0jjmMx5rulg8Qxj6NJ1FtNz5tDz0mnLQOaRw810z2iqDie83+oTG+09QatafiGn+FIjOPCXjKKr7PYp9wIMY4NJDXzu5t+laDQfzO+EinbD2fjMI+TBTeM+g8usZd2Spb7UW/8LO4W+icz2ubxoDueVZnMrF51bDYx5u9rCS50nA0s2OFi2IYPje915LPZ7EtJNgS0iNntDXN/VcE5FfU/wDVVA64Y9zx6Lk+0eEP/r1B8Hj0TbljonHCU+J2ttAtY04Zpi0NvvaedhbMG+eS+Y2rsXF13uqGkWOjoHsMnAm5NjkTceC+uPtDhP8ARrfM30XJ2/hP9Ov8zPRTfl+FrB8aNkV4Ma7DVG1ASHPa9jg5jsiY63zIsCAe9Ko7AqtqtvReaZNje4IAtd4jci+dhY27dV9m7b2EtlTrX/3MU7tt0OpV8Wq+ry/Bph8pMHs+sGCLKobd1gSLgSOuQQvdwm16MB0KnHi3mexar6vJ8Jpj8vgg5bMLgVj1j4D0Qax4PPku1OdmSHJbLs8lw156/mu2En9Z8VR0H9gWteeXkuS11tX58c0BwGrneKBrXnl5f0uw88vL+kpzTa4v4ldNp5Zn93qp4DGvfy8l2Kj1M9oHb3kJlJrDy8UDw9yDUdzHiFkKdv8A4kue3kPqoHb13Z4j1QajuY8QinUZoWn5U8Op8j8pUtU5qO5jxCBUd1h4hPdUHBhPcgOHUPglwUnmes3yXJeeYVAPGB8Fs7/oPklwVKYuPMf87llz1h/zuVrXjqHyWOf7v09U2gqUgJ5jw/pB+I8D6Ktr+bfoP5QanujxCXBrKNz/AHh5+i5L7/qHgfRWOf7o8QsmOqPmaE2g1lHI9byPoiZ6319FXv7cG/OPVY7ED3fmb6ptBrKUvPW+vos3h6319FWcU33PnC4GKZ7nzBNoNZTbw9byd6IkefkfRPdime4f8guTiqfuDv8A6TaCpIL+Z8j6LJdp8CnnHtHFnn6Lg7RZzb4H0V2Kevs0/ht148D1ihbs3Hg02/l48D1ihLKfNzo++e9votbXojg/5gpwxnFYWU1j7laVnFUuq75lv2qn1D3lStazimONPlbvun+qYcVT6h73OWtxzOFMeJSA6mtk0cEFP3k3qDxKx20fcb4XSWwIuTb6rRUpjIFPAb94O6jflWjaL+TR3JJrt5LRWHIK1Hwn2adoVOQ+UI+21eB8h6Je/HCyPtHwSo+D7M+1VuZ8Aj7TV6xSvtHwRvj/AMCV/R9mGrU6zvErJ1Os7xKUa54ldCt2lT6HZNTmfErC1/M+JXJeUTPagwscuTTctN1llbBA80bsoWOB5JaUw0+1Zu+1dZ8lljyVspwWdqyHauyCsIKWUnrYVrtb/EEhTjCPb+V5+BFwrYlclpVsp57sWWZPa4do0T6WIpu0d3HIqhzL65qSps9pzbkfhl4JaKIBEApJuZ+ZuXNunhwTmODtDdB9Lsto3Te/9xQs2U38Jv8Al+4oUHghdFCEac8UOQhEBWtQhB2sCEIrpaVqEHDkwoQiOKac3RCEIDF21ahSVdcV05CFFYF0FiEGOQUIRGDisQhFLWIQgx64chC0klhBQhELqLyMZk8Wy+GSEKwkvstif9ln+X7nLUIVV//Z",
  "link": "/Facilities/1",
  "yelpID": "baylor-scott-and-white-medical-center-temple-temple",
  "reviewUser": [
    "Chris J.",
    "Patricia T.",
    "Penny J."
  ],
  "reviewRating": "1.6",
  "reviewText": "The Physical therapy (PT) given was outstanding. The nursing staff is very attentive and extremely knowledgeable. I love the convenience of valet parking ."
}

const mockFacilitiesData = [mockFacilityData, mockFacilityData, mockFacilityData, mockFacilityData, mockFacilityData, mockFacilityData, mockFacilityData, mockFacilityData, mockFacilityData];

jest.mock('../api', () => ({
  fetchTherapistData: jest.fn().mockResolvedValue(mockTherapistData),
  fetchSupportGroupData: jest.fn().mockResolvedValue(mockSupportGroupData),
  fetchFacilityData: jest.fn().mockResolvedValue(mockFacilityData),
  fetchTherapistsData: jest.fn().mockResolvedValue(mockTherapistsData),
  fetchFacilitiesData: jest.fn().mockResolvedValue(mockFacilitiesData),
  fetchSupportGroupsData: jest.fn().mockResolvedValue(mockSupportGroupsData),
  fetchContributorData: jest.fn(),
  fetchIssueData: jest.fn()
})
);

afterEach(() => {
  jest.clearAllMocks();
});


it('renders splash page with page buttons displayed', () => {
  const {getByText} = render(
    <MemoryRouter>
      <Splash />
    </MemoryRouter>
  );

  const therapistsLink = getByText('410 Therapists');
  const facilitiesLink = getByText('301 Trauma Facilities');
  const supportGroupsLink = getByText('2851 Trauma Support Groups');

  expect(therapistsLink).toBeInTheDocument();
  expect(facilitiesLink).toBeInTheDocument();
  expect(supportGroupsLink).toBeInTheDocument();


  // Simulate clicks on the links
  // fireEvent.click(therapistsLink);
  // expect(window.location.pathname).toBe('/Therapists');
 
  // fireEvent.click(facilitiesLink);
  // expect(window.location.pathname).toBe('/Facilities');
 
  // fireEvent.click(supportGroupsLink);
  // expect(window.location.pathname).toBe('/SupportGroups');
});

it('renders therapist card with correct data', () => {
  render(<TherapistCard {...mockTherapistData} />);

  expect(screen.getByText('Wendy Aporta')).toBeInTheDocument();
  expect(screen.getByText('Accepted')).toBeInTheDocument();
  expect(screen.getByText('Trauma, PTSD, Military/Veteran\'s Issues, Abuse Survivors, Dissociative Identity Disorder (DID), and more')).toBeInTheDocument();
  expect(screen.getByText('Teenagers (12-18), Young Adults (18-24), Adults (24+), Elders (65+)')).toBeInTheDocument();
  expect(screen.getByText('Female')).toBeInTheDocument();
  expect(screen.getByText('White')).toBeInTheDocument();
  expect(screen.getByText('Plano, TX')).toBeInTheDocument();

  const therapistImage = screen.getByAltText('Wendy Aporta\'s headshot');
  expect(therapistImage).toBeInTheDocument();
  expect(therapistImage).toHaveAttribute('src', mockTherapistData.image);
});

it('renders support group card with correct data', () => {
  render(<TraumaSupportGroupsCard {...mockSupportGroupData} />);

  expect(screen.getByText(mockSupportGroupData.sessionName)).toBeInTheDocument();
  expect(screen.getByText(mockSupportGroupData.sessionHostName)).toBeInTheDocument();
  expect(screen.getByText(mockSupportGroupData.phone)).toBeInTheDocument();

  const supportGroupImage = screen.getByAltText('Ivana Radovancevic\'s headshot');
  expect(supportGroupImage).toBeInTheDocument();
  expect(supportGroupImage).toHaveAttribute('src', mockSupportGroupData.image);
});

it('renders facility card with correct data', () => {
  render(<TraumaFacilityCard {...mockFacilityData} />);

  expect(screen.getByText(mockFacilityData.facilityName)).toBeInTheDocument();
  expect(screen.getByText(mockFacilityData.level)).toBeInTheDocument();
  expect(screen.getByText(mockFacilityData.traumaSupportArea)).toBeInTheDocument();
  expect(screen.getByText(mockFacilityData.reviewRating)).toBeInTheDocument();

  const facilityImage = screen.getByAltText('Image of Baylor Scott & White Medical Center - Temple');
  expect(facilityImage).toBeInTheDocument();
  expect(facilityImage).toHaveAttribute('src', mockFacilityData.image);
});

it('renders therapist instance correctly', async () => {
  fetchTherapistData.mockResolvedValue(mockTherapistData);
  fetchFacilitiesData.mockResolvedValue(mockFacilitiesData);
  fetchSupportGroupsData.mockResolvedValue(mockSupportGroupsData);
  
  await act(() => {
    render(<TherapyInstance />);
  });
  
    expect(fetchTherapistData).toHaveBeenCalledTimes(1);
    expect(fetchFacilitiesData).toHaveBeenCalledTimes(1);
    expect(fetchSupportGroupsData).toHaveBeenCalledTimes(1);
    expect(screen.getByText(mockTherapistData.name)).toBeInTheDocument();
    expect(screen.getByText(mockTherapistData.gender)).toBeInTheDocument();
});

it('renders therapy instance table correctly', async () => {
  fetchTherapistData.mockResolvedValue(mockTherapistData);
  fetchFacilitiesData.mockResolvedValue(mockFacilitiesData);
  fetchSupportGroupsData.mockResolvedValue(mockSupportGroupsData);
  
  await act(() => {
    render(<TherapyInstance />);
  });
  
    expect(screen.getByText("Service")).toBeInTheDocument();
    expect(screen.getByText("Price")).toBeInTheDocument();
    expect(screen.getByText("No Services Listed")).toBeInTheDocument();
    expect(screen.getByText("No Pricings Listed")).toBeInTheDocument();
});

it('renders support group instance correctly', async () => {
  fetchTherapistsData.mockResolvedValue(mockTherapistsData);
  fetchFacilitiesData.mockResolvedValue(mockFacilitiesData);
  fetchSupportGroupData.mockResolvedValue(mockSupportGroupData);
  
  await act(() => {
    render(<SupportGroupInstance/>);
  });
  
    expect(fetchTherapistsData).toHaveBeenCalledTimes(1);
    expect(fetchFacilitiesData).toHaveBeenCalledTimes(1);
    expect(fetchSupportGroupData).toHaveBeenCalledTimes(1);
    expect(screen.getByText(mockSupportGroupData.sessionName)).toBeInTheDocument();
    expect(screen.getByText(mockSupportGroupData.phone)).toBeInTheDocument();
});

it('renders facility instance correctly', async () => {
  fetchTherapistsData.mockResolvedValue(mockTherapistsData);
  fetchFacilityData.mockResolvedValue(mockFacilityData);
  fetchSupportGroupsData.mockResolvedValue(mockSupportGroupsData);
  
  await act(() => {
    render(<FacilityInstance />);
  });
  expect(fetchTherapistsData).toHaveBeenCalledTimes(1);
  expect(fetchFacilityData).toHaveBeenCalledTimes(1);
  expect(fetchSupportGroupsData).toHaveBeenCalledTimes(1);
  
  expect(screen.getByText(mockFacilityData.facilityName)).toBeInTheDocument();
  expect(screen.getByText(mockFacilityData.level)).toBeInTheDocument();
  expect(screen.getByText(mockFacilityData.traumaSupportArea)).toBeInTheDocument();
});

it('renders about page', () => {
  render(<About />);

  const postmanLink = screen.getByText('Postman API Documentation');
  const aboutHeader = screen.getByText('About Us');
  const projectTools = screen.getByText('Project Tools');
  const tool = screen.getByText('Google Maps API');
  expect(postmanLink).toBeInTheDocument();
  expect(aboutHeader).toBeInTheDocument();
  expect(projectTools).toBeInTheDocument();
  expect(tool).toBeInTheDocument();
  fireEvent.click(postmanLink);

});

// it('displays the correct number of cards on Support Groups page', async () => {
  
//   fetchSupportGroupsData.mockResolvedValue(mockSupportGroupsData);

//   await act(() => {
//     render(<SupportGroupModel />);
//   });

//   const cards = screen.getAllByTestId('support-group-card');
//   expect(cards).toHaveLength(9); 
// });

it('displays the correct number of cards on Facilities page', async () => {
    
    fetchFacilitiesData.mockResolvedValue(mockFacilitiesData);
  
    await act(() => {
      render(<FacilityModel />);
    });

    const cards = screen.getAllByTestId('facility-card');
    expect(cards).toHaveLength(9);
  });

  // it('displays the correct number of cards on Therapists page', async () => {
  
  //   fetchTherapistsData.mockResolvedValue(mockTherapistsData);
  
  //   await act(() => {
  //     render(<TherapistModel/>);
  //   });
  
  //   const cards = screen.getAllByTestId('therapists-card');
  //   expect(cards).toHaveLength(9);
  // });

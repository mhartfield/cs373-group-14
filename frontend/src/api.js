import axios from "axios";

export const fetchTherapistsData = async (page) => {
    try {
        const result = await axios.get(
            `https://apitexastrauma.support/Therapists/${page}`,
            {}
        );

        let therapists = result.data;
        console.log("RESULT", therapists);
        return therapists;
    }
    catch (e) {
        console.log("Error fetching API:", e)
    }
}

export const fetchSupportGroupsData = async (page) => {
    try {
        const result = await axios.get(
            `https://apitexastrauma.support/SupportGroups/${page}`,
            {}
        );

        let supportGroups = result.data;
        console.log("RESULT", supportGroups);
        return supportGroups;
    }
    catch (e) {
        console.log("Error fetching API:", e)
    }
}

export const fetchFacilitiesData = async (page) => {
    try {
        const result = await axios.get(
            `https://apitexastrauma.support/Facilities/${page}`,
            {}
        );

        let facilities = result.data;
        console.log("RESULT", facilities);
        return facilities;
    }
    catch (e) {
        console.log("Error fetching API:", e)
    }
}

export const fetchTherapistData = async (id) => {
    try {
        const result = await axios.get(
            `https://apitexastrauma.support/Therapists/id/${id}`,
            {}
        );

        let therapist = result.data;
        return therapist;
    }
    catch (e) {
        console.log("Error fetching API:", e)
    }
}

export const fetchSupportGroupData = async (id) => {
    try {
        const result = await axios.get(
            `https://apitexastrauma.support/SupportGroups/id/${id}`,
            {}
        );

        let supportGroup = result.data;
        return supportGroup;
    }
    catch (e) {
        console.log("Error fetching API:", e)
    }
}

export const fetchFacilityData = async (id) => {
    try {
        const result = await axios.get(
            `https://apitexastrauma.support/Facilities/id/${id}`,
            {}
        );

        let facility = result.data;
        return facility;
    }
    catch (e) {
        console.log("Error fetching API:", e)
    }
}

export const fetchFacilityReviewData = async (id) => {
    try {
        const result = await axios.get(
            `https://apitexastrauma.support/Reviews/${id}`,
            {}
        );

        let reviews = result.data;
        return reviews;
    }
    catch (e) {
        console.log("Error fetching API:", e)
    }
}

export const fetchContributorData = async (projectId) => {
    try {
        const result = await axios.get(
            `https://gitlab.com/api/v4/projects/${projectId}/repository/contributors`,
            {}
        );

        return result.data;
    }
    catch (e) {
        console.log("Error fetching Gitlab Contributor info:", e)
    }
}

export const fetchIssueData = async (projectId) => {
    try {
        const result = await axios.get(
            `https://gitlab.com/api/v4/projects/${projectId}/issues?state=closed&per_page=100`,
            {}
        );
        return result.data;
    }
    catch (e) {
        console.log("Error fetching Gitlab Issues info:", e)
    }
}
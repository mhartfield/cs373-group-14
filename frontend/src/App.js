import "./App.css";
import Navbar from "./components/Navbar";
import Splash from "./pages/Splash";
import About from "./pages/About";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import TherapistModel from "./pages/models/TherapistModel";
import FacilityModel from "./pages/models/FacilityModel";
import SupportGroupModel from "./pages/models/SupportGroupModel";
import TherapistInstance from "./pages/instances/TherapistInstance";
import FacilityInstance from "./pages/instances/FacilityInstance";
import SupportGroupInstance from "./pages/instances/SupportGroupInstance";

function App() {
  document.title = 'Texas Trauma Support';

  return (
    <>
      <Router>
        <Navbar />
        <Routes>
          <Route path="/" Component={Splash} />
          <Route path="/About" Component={About} />
          <Route path="/Therapists" Component={TherapistModel} />
          <Route path="/Facilities" Component={FacilityModel} />
          <Route path="/SupportGroups" Component={SupportGroupModel} />
          <Route path="/Therapists/:id" Component={TherapistInstance} />
          <Route path="/Facilities/:id" Component={FacilityInstance} />
          <Route path="/SupportGroups/:id" Component={SupportGroupInstance} />
        </Routes>
      </Router>
    </>
  );
}

export default App;

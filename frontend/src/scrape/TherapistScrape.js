const puppeteer = require("puppeteer");
const fs = require("fs");

(async () => {
  // Launch the browser and open a new blank page
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  page.setDefaultTimeout(3000);
  // Set screen size
  await page.setViewport({ width: 1080, height: 1024 });
  let therapistData = [];
  let id = 1;
  let pages = 1;
  while (pages <= 42) {
    await page.goto(
      "https://meetmonarch.com/therapists?location=Texas&page=" +
        pages +
        "&specialty=Trauma"
    );
    let therapists = await page.$$(".result-name");
    let length = therapists.length;

    for (let i = 0; i < length; i++) {
      therapists = await page.$$(".result-name");
      const profilePage = await therapists[i].$eval(
        ":scope > *",
        (childElement) => {
          return childElement ? childElement.getAttribute("href") : null;
        }
      );
      await page.goto("https://meetmonarch.com" + profilePage);

      // name
      try {
        await page.waitForSelector(".clinician-name-container h1");
      } catch {
        await page.reload();
      }
      let name = await page.$eval(".clinician-name-container h1", (element) =>
        element.textContent.trim()
      );
      // insurance
      let insurance = await page.$(".no-insurance-header");
      insurance = !insurance ? "Accepted" : "Not Accepted";
      // specialties
      let specialties = await page.$eval(
        ".details-text span:nth-child(2)",
        (element) => element.textContent.trim()
      );
      if (specialties.endsWith(",")) {
        specialties += " and more";
      }
      // targetAgeGroup
      const ageGroups = await page.evaluate(() => {
        const ageGroupsSection = Array.from(
          document.querySelectorAll(".client-focus-list h3.title")
        ).find((section) => section.textContent.trim() === "Age groups");

        if (!ageGroupsSection) {
          return "Any";
        }

        const ageGroupElements = Array.from(
          ageGroupsSection.nextElementSibling.querySelectorAll("span")
        );
        const ageGroupsArray = ageGroupElements.map((span) =>
          span.textContent.trim()
        );
        return ageGroupsArray.join(", ");
      });
      // gender
      let pronouns = await page.$(".single-pronoun");
      let gender = "Not specified";
      if (pronouns) {
        pronouns = await page.$eval(".single-pronoun", (element) =>
          element.textContent.trim().toLowerCase()
        );
        if (pronouns.includes("she")) {
          gender = "Female";
        } else if (pronouns.includes("he")) {
          gender = "Male";
        }
      }
      if (gender == "Not specified") {
        let tempGender = await page.$(".clinician-gender-container .content");
        if (tempGender) {
          gender = await page.$eval(
            ".clinician-gender-container .content",
            (element) => element.textContent.trim().toUpperCase()
          );
        }
      }
      // raceEthnicity
      let raceEthnicity = await page.$(".clinician-race-container .content");
      if (raceEthnicity) {
        raceEthnicity = await page.$eval(
          ".clinician-race-container .content",
          (element) => element.textContent.trim()
        );
      } else {
        raceEthnicity = "Not specified";
      }
      // location
      let location = await page.$(".location-address");
      if (location) {
        location = await page.$eval(".location-address", (element) =>
          element.textContent.trim()
        );
        location = location.split(",")[0];
      } else {
        location = "Not specified";
      }
      // image
      let image = await page.$("._picture-img_5gyo0a img:last-child");
      if (image) {
        image = await page.$eval("._picture-img_5gyo0a img:last-child", (img) =>
          img.getAttribute("src")
        );
      } else {
        image = "";
      }
      // link
      let link = "/Therapists/" + id;
      // pricingOptions
      const payment = await page.$(".payment-option-list:last-of-type");
      let services = await payment.$$eval("ul li", (items) => {
        return items.map((item) => {
          const textContent = item.querySelector(
            "div.info-list-item > span"
          ).textContent;
          const indexOfDollar = textContent.indexOf("$");
          const service = textContent.slice(0, indexOfDollar).trim();
          const price = textContent.slice(indexOfDollar).trim();
          return { service, price };
        });
      });

      // intro
      let intro = await page.$eval(".section-text", (element) =>
        element.textContent.trim()
      );
      // dataFaq
      const faqs = await page.$$eval(".faq-container", (faqContainers) => {
        return faqContainers.map((container) => {
          const question = container
            .querySelector(".question")
            .textContent.trim();
          const answer = container.querySelector(".answer").textContent.trim();
          return { question, answer };
        });
      });

      // Go back to the previous page for the next iteration
      await page.evaluate(() => window.history.back());

      // Wait for the previous page to load
      await page.waitForNavigation({
        waitUntil: "domcontentloaded",
        timeout: 5000,
      });
      console.log(id);
      id += 1;
      therapistData.push({
        name: name,
        insurance: insurance,
        specialties: specialties,
        targetAgeGroup: ageGroups,
        gender: gender,
        raceEthnicity: raceEthnicity,
        location: location,
        image: image,
        link: link,
        pricingOptions: services,
        intro: intro,
        dataFaq: faqs,
      });
    }
    pages++;
  }

  //create json file
  fs.writeFileSync("therapists.json", JSON.stringify(therapistData, null, 2));

  // Close the browser
  await browser.close();
})();

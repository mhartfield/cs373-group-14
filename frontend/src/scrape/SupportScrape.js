const puppeteer = require("puppeteer");
const fs = require("fs");

(async () => {
  // Launch the browser and open a new blank page
  const browser = await puppeteer.launch({
    timeout: 0,
    defaultViewport: null,
    args: ["--no-sandbox", "--disable-setuid-sandbox"],
  });
  const page = await browser.newPage();
  page.setDefaultTimeout(3000);
  // Set screen size
  await page.setViewport({ width: 1080, height: 1024 });
  let supportData = [];
  let id = 1;
  let pages = 1;
  while (pages <= 143) {
    console.log("PAGE: " + pages);
    await page.goto(
      "https://www.psychologytoday.com/us/groups/texas?page=" + pages,
      { timeout: 10000000 }
    );
    let hrefs = await page.$$eval(".results-row-info > a", (anchors) =>
      anchors.map((anchor) => anchor.getAttribute("href"))
    );
    let length = hrefs.length;
    for (let i = 0; i < length; i++) {
      hrefs = await page.$$eval(".results-row-info > a", (anchors) =>
        anchors.map((anchor) => anchor.getAttribute("href"))
      );
      // location
      let location = await page.$$eval(".address", (elements) => {
        return elements.map((element) => {
          return element.textContent.trim();
        });
      });
      if (location[i]) {
        let startIndex =
          location[i].indexOf("Group meets in ") + "Group meets in ".length;
        let endIndex = location[i].indexOf(",");
        location = location[i].slice(startIndex, endIndex).trim();
      } else {
        location = "Not specified";
      }
      // console.log("\nLocation: " + location);
      await page.goto(hrefs[i]);
      // sessionHostName
      let sessionHostName = await page.$(".hosted-by");
      if (sessionHostName) {
        sessionHostName = await page.$eval(".hosted-by", (element) => {
          return element.textContent.trim();
        });
      } else {
        sessionHostName = "Not specified";
      }
      // console.log("Session Host Name: " + sessionHostName);
      // address
      let address = await page.$(".address");
      if (address) {
        address = await page.$eval(".address", (element) =>
          element.textContent.trim()
        );
      } else {
        address = "Not specified";
      }
      address = address.replace(/\s+/g, " ");
      // console.log("Address: " + address);
      // sessionName
      let sessionName = await page.$(".contact-bar-name");
      if (sessionName) {
        sessionName = await page.$eval(".contact-bar-name", (element) =>
          element.textContent.trim()
        );
      } else {
        sessionName = "Not specified";
      }
      // console.log("Session Name: " + sessionName);
      // price
      let price = await page.$(".group-price");
      if (price) {
        price = await page.$eval(".group-price", (element) =>
          element.textContent.trim()
        );
      } else {
        price = "Not specified";
      }
      let num = 0;
      const matches = price.match(/\$(\d+)/);
      if (matches) {
        const leftNumber = parseInt(matches[1], 10);
        if (leftNumber < 90) {
          num = 0;
        } else if (leftNumber >= 90 && leftNumber < 130) {
          num = 1;
        } else {
          num = 2;
        }
      }
      price = num;
      // console.log("Price: " + price);
      // traumaIssue
      let traumaIssue = await page.evaluate(() => {
        const titles = Array.from(
          document.querySelectorAll(".group-attributes-title")
        );
        const expertiseTitle = titles.find((title) =>
          title.textContent.includes("Expertise")
        );
        if (!expertiseTitle) {
          return "Not specified";
        }
        return expertiseTitle.nextElementSibling.textContent.trim();
      });
      traumaIssue = traumaIssue.replace(/\s+/g, " ");
      traumaIssue = traumaIssue.split(", ").map((item) => item.trim());
      // console.log("Expertise: " + JSON.stringify(traumaIssue));
      // image
      let image = await page.$(".groups-profile-photo img");
      if (image) {
        image = await page.$eval(".groups-profile-photo img", (img) =>
          img.getAttribute("src")
        );
      } else {
        image = "";
      }
      // console.log("Image: " + image);
      // link
      let link = "/SupportGroups/" + id;
      // console.log("Link: " + link);
      // phone
      let phone = await page.$(".lets-connect-phone-number");
      if (phone) {
        phone = await page.$eval(".lets-connect-phone-number", (element) =>
          element.textContent.trim()
        );
      } else {
        phone = "Not specified";
      }
      // console.log("Phone: " + phone);
      // sessionDesc
      let sessionDesc = await page.$(".group-description");
      if (sessionDesc) {
        sessionDesc = await page.$eval(".group-description", (element) =>
          element.textContent.trim()
        );
        sessionDesc = sessionDesc.replace(/\s+/g, " ");
        if (!sessionDesc) {
          sessionDesc = "Not specified";
        }
      } else {
        sessionDesc = "Not specified";
      }

      // console.log("Session Description: " + sessionDesc);
      // meetingTime
      let meetingTime = await page.$(".schedule-display");
      if (meetingTime) {
        meetingTime = await page.$eval(".schedule-display", (element) =>
          element.textContent.trim()
        );
      } else {
        meetingTime = "Not specified";
      }
      // console.log("Meeting time: " + meetingTime);
      // website
      let website = await page.evaluate(() => {
        const buttons = Array.from(document.querySelectorAll(".btn"));
        const websiteButton = buttons.find((button) =>
          button.textContent.includes("My website")
        );

        if (!websiteButton) {
          return "Not specified";
        }

        const websiteLink = websiteButton.querySelector("a");
        const websiteHref = websiteLink
          ? websiteLink.getAttribute("href")
          : null;
        return websiteHref ? websiteHref.trim() : "Not specified";
      });
      if (website !== "Not specified") {
        let old = "" + page.url();
        try {
          await page.goto(website, {
            timeout: 10000000,
            waitUntil: "load",
          });
          website = page.url();
          await page.goto(old, {
            timeout: 10000000,
            waitUntil: "load",
          });
        } catch (error) {
          console.error("Error navigating to website:", error.message);
          website = "Not specified";
          await page.goto(old, {
            timeout: 10000000,
            waitUntil: "load",
          });
        }
      }
      // console.log("Website: " + website);
      // Go back to the previous page for the next iteration
      await page.goto(
        "https://www.psychologytoday.com/us/groups/texas?page=" + pages,
        { timeout: 10000000 }
      );
      console.log(id);
      id += 1;
      supportData.push({
        sessionHostName: sessionHostName,
        location: location,
        address: address,
        sessionName: sessionName,
        price: price,
        traumaIssue: traumaIssue,
        image: image,
        link: link,
        phone: phone,
        sessionDesc: sessionDesc,
        meetingTime: meetingTime,
        website: website,
      });
    }
    pages++;
  }
  //create json file
  fs.writeFileSync("SupportGroups.json", JSON.stringify(supportData, null, 2));

  // Close the browser
  await browser.close();
})();

//changed traumaIssue!!!!!!!!!

import React from "react";

const ToolCard = ({toolName, img}) => {
return (

<div class="card">
  <img class="card-img-top" 
  src={img} 
  alt={toolName}
  style={{ height: "200px", objectFit: "contain" }}/>
  <div class="card-body">
    <h5 class="card-title" style={{textAlign: "center"}}>{toolName}</h5>
  </div>
</div>
)
};

export default ToolCard;


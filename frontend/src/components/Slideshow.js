import Acute from "../images/Acute Trauma.jpeg";
import Chronic from "../images/Chronic Trauma.jpeg";
import Complex from "../images/Complex Trauma.jpeg";

function Slideshow() {
  return (
    <>
      <div id="carouselExampleCaptions" className="carousel slide">
        <div className="carousel-indicators">
          <button
            type="button"
            data-bs-target="#carouselExampleCaptions"
            data-bs-slide-to="0"
            className="active"
            aria-current="true"
            aria-label="Slide 1"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleCaptions"
            data-bs-slide-to="1"
            aria-label="Slide 2"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleCaptions"
            data-bs-slide-to="2"
            aria-label="Slide 3"
          ></button>
        </div>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img 
            style={{"border-radius": "0.5rem"}}
            src={Acute}
            className="d-block w-100 carousel-img"
            alt="..." />
            <div className="carousel-caption d-none d-md-block">
              <h5>Acute Trauma</h5>
              <p className="text-white">
                Comes from a single, unexpected, and stressful event.{" "}
              </p>
            </div>
          </div>
          <div className="carousel-item">
            <img
              style={{"border-radius": "0.5rem"}}
              src={Chronic}
              className="d-block w-100 carousel-img"
              alt="..."
            />
            <div className="carousel-caption d-none d-md-block">
              <h5>Chronic Trauma</h5>
              <p className="text-white">
                Comes from repeated traumatic events.{" "}
              </p>
            </div>
          </div>
          <div className="carousel-item">
            <img
            style={{"border-radius": "0.5rem"}}
              src={Complex}
              className="d-block w-100 carousel-img"
              alt="..."
            />
            <div className="carousel-caption d-none d-md-block">
              <h5>Complex Trauma</h5>
              <p className="text-white">
                Involves multiple different kinds of traumatic events that
                combine to create unique trauma symptoms.{" "}
              </p>
            </div>
          </div>
        </div>
        <button
          className="carousel-control-prev"
          type="button"
          data-bs-target="#carouselExampleCaptions"
          data-bs-slide="prev"
        >
          <span
            className="carousel-control-prev-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button
          className="carousel-control-next"
          type="button"
          data-bs-target="#carouselExampleCaptions"
          data-bs-slide="next"
        >
          <span
            className="carousel-control-next-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
    </>
  );
}
export default Slideshow;

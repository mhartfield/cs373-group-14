const TherapistCard = ({
  name,
  insurance,
  specialties,
  targetAgeGroup,
  gender,
  raceEthnicity,
  location,
  image,
  id,
}) => {
  const handleClick = () => {
    window.location.href = `/Therapists/${id}`; // Redirect to the specified link
  };
  return (
    <div
      className="card card-instance"
      onClick={handleClick}
      style={{ height: "700px" }}
    >
      <img
        src={image}
        className="card-img-top"
        alt={`${name}'s headshot`}
        style={{ height: "400px", objectFit: "contain" }}
      />
      <div className="card-body">
        <h2 className="card-title">{name}</h2>
        <p className="card-text">
          <strong>Insurance:</strong> {insurance ? "Accepted" : "Not Covered"}
        </p>
        <p className="card-text">
          <strong>Specialties:</strong> {specialties.join(", ")}
        </p>
        <p className="card-text">
          <strong>Target Age Group:</strong> {targetAgeGroup.join(", ")}
        </p>
        <p className="card-text">
          <strong>Gender:</strong> {gender}
        </p>
        <p className="card-text">
          <strong>Race & Ethnicity:</strong> {raceEthnicity}
        </p>
        <p className="card-text">
          <strong>City:</strong> {location}, TX
        </p>
      </div>
    </div>
  );
};
export default TherapistCard;

import React from "react";

const TeamCard = ({ name, commits, issues, moreInfo }) => {

  return (
      <div class="card" style={{ height: "700px", objectFit: "cover" }}>
        <img
          src={moreInfo.img}
          class="card-img-top"
          alt={`${name}`}
          style={{ height: "300px", objectFit: "cover" }}
        />
        <div class="card-body">
          <h2 class="card-title">{moreInfo.fullName}</h2>
          <p class="card-text">{moreInfo.bio}</p>
          <p class="card-text"><strong>Role:</strong> {moreInfo.role}</p>
          <p>Number of Commits: {commits}<br></br>
          Number of Issues: {issues}<br></br>
          Number of Unit Tests: {moreInfo.unittests}</p> 
        </div>
      </div>

  );
};

export default TeamCard;

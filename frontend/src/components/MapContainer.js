import React, { useState, useEffect } from 'react';
import { GoogleMap, useJsApiLoader, Marker } from '@react-google-maps/api';

const MapContainer = ({ location }) => {
    const {isLoaded} = useJsApiLoader({
        googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_API_KEY });
    const [markerPosition, setMarkerPosition] = useState(null);
  
    useEffect(() => {
      if (isLoaded) {
        const geocoder = new window.google.maps.Geocoder();
        geocoder.geocode({ address: location }, (results, status) => {
          if (status === 'OK') {
            const { lat, lng } = results[0].geometry.location;
            setMarkerPosition({ lat: lat(), lng: lng() });
          } else {
            console.error('Geocode unsuccessful:', status);
          }
        });
      }
  
    }, [isLoaded]);
  
    return (isLoaded ? 
      (<div style={{ height: '400px', width: '100%' }}>
        <GoogleMap
          mapContainerStyle={{ height: '100%', width: '100%' }}
          zoom= {50}
          center={markerPosition}
        >
          {markerPosition && <Marker position={markerPosition} />}
        </GoogleMap>
      </div>) : "Loading..."
    );
  };

export default MapContainer;
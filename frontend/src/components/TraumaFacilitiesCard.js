const TraumaFacilityCard = ({
  facilityName,
  city,
  level,
  traumaSupportArea,
  image,
  reviewRating,
  id
}) => {
  const handleClick = () => {
    window.location.href = `/Facilities/${id}`; // Redirect to the specified link
  };
  return (
      <div 
      class="card card-instance" 
      onClick={handleClick}
      style={{ height: "700px"}}>
        <img
          src={image}
          class="card-img-top"
          alt={`Image of ${facilityName}`}
          style={{ height: "300px", objectFit: "cover" }}/>
        <div class="card-body">
          <h2 class="card-title">{facilityName}</h2>
          <p class="card-text">
            <strong>Level:</strong> {level}
          </p>
          <p class="card-text">
            <strong>Trauma Support Area:</strong> {traumaSupportArea}
          </p>
          <p class="card-text">
            <strong>City:</strong> {city}, TX
          </p>
          <p class="card-text">
            <strong>Rating:</strong> {reviewRating}
          </p>
        </div>
      </div>
  );
};
export default TraumaFacilityCard;

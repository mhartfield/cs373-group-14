// PricingTable.js

import React from "react";

const PricingTable = ({ services, prices }) => {
  console.log("Services", services);
  return (
    <div className="pricing-table">
      <table>
        <thead>
          <tr>
            <th>Service</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {services.length > 0 ? services.map((service, index) => (
            <tr key={index}>
              <td>{service}</td>
              <td>{prices[index]}</td>
            </tr>
          )) : 
          (<tr key={0}>
          <td>No Services Listed</td>
          <td>No Pricings Listed</td>
        </tr>)}
        </tbody>
      </table>
    </div>
  );
};

export default PricingTable;

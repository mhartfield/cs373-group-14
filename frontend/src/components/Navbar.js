import { Link } from "react-router-dom";
import { default as nb } from "react-bootstrap/Navbar";



function Navbar() {
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <nb.Brand href="/">TTS</nb.Brand>
          <div className="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
            <div className="navbar-nav">
              <Link 
              className="nav-link nav-item" 
              aria-current="page" to="/">
                Home
              </Link>
              <Link 
              className="nav-link nav-item" 
               to="/About">
                About
              </Link>
              <Link 
              className="nav-link nav-item" 
              to="/Therapists">
                Therapists
              </Link>
              <Link 
              className="nav-link nav-item" 
              to="/Facilities">
                Trauma Facilities
              </Link>
              <Link 
              className="nav-link nav-item" 
              to="/SupportGroups">
                Trauma Support Groups
              </Link>
            </div>
          </div>
        </div>
      </nav>
    </>
  );
}
export default Navbar;

const TraumaSupportGroupsCard = ({
  sessionHostName,
  location,
  sessionName,
  price,
  traumaIssue,
  image,
  id,
  phone,
}) => {
  const handleClick = () => {
    window.location.href = `/SupportGroups/${id}`; // Redirect to the specified link
  };
  return (
      <div
      class="card card-instance" 
      onClick={handleClick}
      style={{ height: "700px" }}>
        <img
          src={image}
          class="card-img-top"
          alt={`${sessionHostName}'s headshot`}
          style={{ height: "450px", objectFit: "contain" }}
        />
        <div class="card-body">
          <h2 class="card-title">{sessionHostName}</h2>
          <p class="card-text">
            <strong>City:</strong> {location}, TX
          </p>
          <p class="card-text">
            <strong>Session Name:</strong> {sessionName}
          </p>
          <p class="card-text">
            <strong>Price:</strong> ${price}
          </p>
          <p class="card-text">
            <strong>Trauma Focus:</strong>
            <p>{traumaIssue.join(", ")}</p>
          </p>
          <p class="card-text">
            <strong>Phone Number:</strong> {phone}
          </p>
        </div>
      </div>
  );
};
export default TraumaSupportGroupsCard;

import React, { useEffect, useState } from 'react';

const YelpContainer = ({ facility }) => {
  const apiKey = process.env.REACT_APP_YELP_API_KEY;
  const businessId = facility.yelpID;
  
  const [reviews, setReviews] = useState([]);
  const [errorStatusCode, setErrorStatusCode] = useState(null);

  useEffect(() => {
    const fetchYelpReviews = async () => {
      try {
        console.log("businessID",businessId);
        const response = await fetch(`https://api.yelp.com/v3/businesses/${businessId}/reviews`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${apiKey}`,
          },
        });

        console.log('Request URL:', `https://api.yelp.com/v3/businesses/${businessId}/reviews`); // Log the request URL

        if (!response.ok) {
          setErrorStatusCode(response.status);
          throw new Error('Failed to fetch reviews');
        }

        const data = await response.json();
        console.log('Yelp Reviews Data:', data); // Log the Yelp reviews data
        setReviews(data.reviews);
      } catch (error) {
        console.error('Error fetching Yelp reviews:', error.message);
      }
    };

    // Fetch Yelp reviews
    fetchYelpReviews();
  }, [businessId, apiKey]);

  return (
    <div>
      {/* <h1>Yelp Reviews</h1> */}
      {errorStatusCode ? (
        <p>{`Yelp Reviews Return: ${errorStatusCode}`}</p>
      ) : reviews.length === 0 ? (
        <p>No reviews available.</p>
      ) : (
        <div>
          {reviews.slice(0, 3).map((review, index) => (
            <div key={index}>
              <p><strong>{review.user.name}</strong></p>
              <p>{review.text}</p>
              <hr />
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default YelpContainer;
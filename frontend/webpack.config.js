module.exports = {
    // Other webpack configuration options...
  
    module: {
      rules: [
        // Rule for CSS files
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        },
  
        // Other rules as needed...
      ],
    },
  };